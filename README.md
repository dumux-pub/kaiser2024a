# A Mixed-Dimensional Model for Evapotranspiration from Leaves

This repository contains the source code for the master thesis "A Mixed-Dimensional Model for Evapotranspiration from Leaves" by Lars Kaiser, April 2024.

The source code implements a fully-coupled mixed-dimensional model for whole-leaf evapotranspiration with explicit representation of water pathways in the xylem network and the non-vascular tissue. The model represents the veins as 1d-network of tube segments, and the non-vascular leaf tissue as a porous medium in 3d, and couples the two compartments using a meniscus model based on a chemical equilibrium. We incorporate external environment variables into the boundary conditions of the simulation.

The source code contains two parts:

- the vein growth algorithm to generate an artificial vein network and
- the mixed-dimension flow model to simulate the leaf.

Installation
-----------------

The leaf simulation can be installed by running

```bash
python install_kaiser2024a.py
```

However, the growth algorithm needs [CGAL](https://www.cgal.org/), which is now header-only.

Manual Installation
--------------------

A manual installation needs dumux (3.8 or master), dune (2.9 or master), dune-foamgrid and dune-alugrid as grid managers in matching version.
Installation instructions are available on the [DuMuX Repository](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux).

Vein growth algorithm
-----------------------

In `appl/veins`, the executable is called `vein_generator` and can be compiled with the command `make vein_generator`.
The source code implements a leaf vein generation algorithm following the paper by [Runions et al (2005)](https://doi.org/10.1145/1073204.1073251) ACM Transactions on Graphics.

Mixed-dimension flow model
----------------------------

In `appl/1d3d`, the executable is called `leaf_evaporation` and can be compiled with the command `make leaf_evaporation`.
It can be started from a subdirectory, e.g. by running

```bash
mkdir results && cd results
../leaf_evaporation ../inputs/small_leaf-params.input
```

Run Times
----------

The run times in the study were achieved by enabling the multithreading capabilities of the SubGrid Class in `dune-subgrid/dune/subgrid/subgrid.hh`:

```cpp
namespace Capabilities {

// ...

//! \brief FoamGrid is thread-safe for grid views
template<int dim, class HostGrid, bool MapIndexStorage>
struct viewThreadSafe< SubGrid<dim, HostGrid, MapIndexStorage> > {
  static const bool v = true;
};

// ...

}
```

and using a parallel SOR method `dumux/linear/seqsolverbackend.hh`:

replace

```cpp
    template<class M, class X, std::size_t i>
    using Smoother = Dune::SeqSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
```

by

```cpp
    template<class M, class X, std::size_t i>
    using Smoother = Dumux::ParMTSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
```

Both changes will also be applied when using the installation script `install_kaiser2024a.py` to install this application.
