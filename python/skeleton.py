from skimage.morphology import skeletonize
from skimage import io
from skimage.util import img_as_ubyte, img_as_bool
import matplotlib.pyplot as plt

SHOW_IMAGES = False

import PIL.Image
PIL.Image.MAX_IMAGE_PIXELS = None

# open and skeletonize
print("Read image")
img = io.imread("binarized.png")
print("Skeletonize")
skeleton = skeletonize(img_as_bool(img))
ske = img_as_ubyte(skeleton)
ske[ske == 255] = 1
print("Save skeleton")
io.imsave("skeleton.png", ske)

if SHOW_IMAGES:
    cols, rows = 1, 1
    fig, axes = plt.subplots(rows, cols, figsize=(4*cols, 4*rows))
    axes.imshow(img + ske)
    fig.tight_layout()
    plt.show()
