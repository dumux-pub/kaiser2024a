import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from scipy import ndimage

from skimage import io

import sknw_large as sknw

from simplify_polyline import avg, simplify_polyline
from write_vtp import write_vtp

import PIL.Image
PIL.Image.MAX_IMAGE_PIXELS = 379214325+10

# build graph from skeleton
print("Read skeleton from file")
ske = io.imread("skeleton.png")
print("Compute graph from skeleton")
ske = ske.astype(np.int64)
graph = sknw.build_sknw(ske, iso=False, ring=False)

print("Compute distance transform")
img = io.imread("binarized.png")
distance = ndimage.distance_transform_edt(img)

# compute the mean radius of each edge
nx.set_edge_attributes(
    graph,
    {
        (start, end): dict(
            ed, **{"R": np.mean(distance[ed["pts"][:, 0], ed["pts"][:, 1]])}
        )
        for start, end, ed in graph.edges.data()
    },
)

# simplify graph
nodes = graph.nodes()
vertices_array = [nodes[i]["o"] for i in nodes]
connectivity_array = []
radius_array = []

# go over direct edges (bifurcation->bifurcation) and add tortuous points for each segment
#
# B.___________________________________.B    direct edge
#   \____                  ___    ____/
#         \_______________/   \__/           tortuous edges
#
print("-- Processing edges...")
for start, end, data in graph.edges.data():
    polyline = [vertices_array[start], *data["pts"], vertices_array[end]]
    firstIndex = len(vertices_array)
    radius = data["R"]

    # simplify the polyline
    # these procedures only make sense if we have at least 3 points
    numPoints = len(polyline)
    if numPoints >= 3:
        # smoothing (use the midpoints between points)
        polyline = [polyline[0], *[avg(polyline[i], polyline[i+1]) for i in range(numPoints-1)], polyline[-1]]
        # simplify use radius as coarsening criterion
        polyline = simplify_polyline(polyline, epsilon=radius)
        assert len(polyline) <= numPoints

    # new points and point data only if there is tortuous segments
    if len(polyline) > 2:
        vertices_array += polyline[1:-1]

    # append tortuous edges
    assert len(polyline) >= 2
    if len(polyline) == 2:
        new_cells = [(start, end)]
    elif len(polyline) == 3:
        new_cells = [(start, firstIndex), (firstIndex, end)]
    else:
        new_cells = [(start, firstIndex), *[(firstIndex+i, firstIndex+i+1) for i in range(len(polyline)-3)], (firstIndex+len(polyline)-3, end)]

    connectivity_array += new_cells
    radius_array += [radius,]*len(new_cells)

print(f"-- Writing output to network.vtp...")
write_vtp(
    filename="network.vtp",
    vertices_array=vertices_array,
    connectivity_array=connectivity_array,
    point_data=dict(),
    cell_data={"radius": radius_array},
)

# some plotting to be sure everything worked
img = io.imread("../Fagus-12-32x_p193.JPG")
plt.imshow(img, alpha=1.0)

for start, end, ed in graph.edges().data():
    points = graph[start][end]["pts"]
    plt.plot(points[:, 1], points[:, 0], "green")

# draw node by o
nodes = graph.nodes()
points = np.array([nodes[i]["o"] for i in nodes])
# big nodes
plt.plot(points[:, 1], points[:, 0], "r.")
# all nodes in simplified graph
vertices_array = np.array(vertices_array)
plt.plot(vertices_array[:, 1], vertices_array[:, 0], "b.")

# title and show
plt.show()
