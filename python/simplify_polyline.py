"""
Simplify a polyline using the Douglas Peucker algorithm
# see http://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
Author: Timo Koch
"""
import numpy as np
from functools import partial


def distance(v1, v2):
    """Distance between two vertices"""
    return np.linalg.norm(np.array(v1)-np.array(v2), ord=2)


def avg(v1, v2):
    """Arithmetic mean of two vertices"""
    return (0.5*(np.array(v1)+np.array(v2))).tolist()


def douglas_peucker_mask(polyline, epsilon, dist):
    """Mask the points that can be dropped for simplification"""
    mask = np.ones(len(polyline), dtype=bool)
    lineStack = [(0, len(polyline)),]
    while lineStack:
        begin, end = lineStack.pop()
        # compute the distances for all in-between points
        line_dist = partial(dist, a=polyline[begin], b=polyline[end-1])
        distances = [line_dist(p) for p in polyline[begin:end]]
        maxIdx = np.argmax(distances)
        maxDist = distances[maxIdx]

        # if deviation to last divide line which adds the division point
        # otherwise mark all in-between points for deletion
        if maxDist > epsilon:
            lineStack += [(begin, begin+maxIdx), (begin+maxIdx, end),]
        else:
            mask[begin+1:end-1] = False

    return mask


def point_line_distance(point, a, b):
    """Distance of point to line through a and b"""
    if np.all(np.equal(a, b)):
        return np.linalg.norm(point - a)

    if len(point) == 2:
        point = np.pad(point, (0, 1))
        a = np.pad(a, (0, 1))
        b = np.pad(b, (0, 1))

    return np.divide(
        np.abs(np.linalg.norm(np.cross(b - a, a - point), ord=2)),
        np.linalg.norm(b - a, ord=2)
    )


def simplify_polyline(polyline, epsilon):
    polyline = np.array(polyline)
    mask = douglas_peucker_mask(polyline, epsilon, dist=point_line_distance)
    return polyline[mask].tolist()
