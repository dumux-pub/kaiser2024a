#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.filters import sato, threshold_multiotsu, rank
from skimage.exposure import equalize_adapthist
from skimage.morphology import disk
from skimage.util import img_as_ubyte
from skimage.color import rgb2gray

SHOW_IMAGES = True

if SHOW_IMAGES:
    numFigs = 6
    cols = int(np.ceil(np.sqrt(numFigs)))
    rows = int(np.floor(numFigs/cols))
    fig, axes = plt.subplots(rows, cols, figsize=(4*cols, 4*rows))
    cmap = plt.cm.gray
    axes = axes.flatten()

print("Read image, crop")
img = io.imread("../whole_leaf_fagus_14_32.tif")
img = rgb2gray(img)
#img = img[:,:,1] # take only red channel
#img = img[300:-300,300:-300]
axes[0].imshow(img, cmap=cmap, aspect='auto')
plt.show()

# median filter (remove noise, preserve edges)
print("Median filter")
img = rank.median(img, disk(2))
if SHOW_IMAGES:
    axes[0].imshow(img, cmap=cmap, aspect='auto')

# local equalize + thresholding gives rough structure (small vessels)
print("Local equalize + thresholding")
footprint = disk(50)
equalA = rank.equalize(img, footprint=footprint)
t0, t1 = threshold_multiotsu(equalA, classes=3)
maskA = (equalA <= t0)
if SHOW_IMAGES:
    axes[1].imshow(maskA, cmap=cmap, aspect='auto')

print("Global equalize + thresholding")
# global equalize + thresholding gives rough structure (big vessels)
equalAA = equalize_adapthist(img)
t0, t1 = threshold_multiotsu(equalAA, classes=3)
maskAA = (equalAA <= t0)
if SHOW_IMAGES:
    axes[2].imshow(maskAA, cmap=cmap, aspect='auto')

print("Sato tubeness + thresholding")
# sato tubeness + thresholding gives tube edges
imgSato = sato(img, sigmas=range(4, 14, 2))
t0, t1 = threshold_multiotsu(imgSato, classes=3)
maskB = (imgSato > t0)
# imgSato2 = sato(img, sigmas=(100, 200))
# t0, t1 = threshold_multiotsu(imgSato2, classes=3)
# maskBB = (imgSato2 > t0)
# maskB |= maskBB
if SHOW_IMAGES:
    axes[3].imshow(maskB, cmap=cmap, aspect='auto')

print("Add masks")
# add all the threshold masks
maskC = maskA | maskAA | maskB
if SHOW_IMAGES:
    axes[4].imshow(maskC, cmap=cmap, aspect='auto')

print("Median filter")
binarized = img_as_ubyte(maskC)
binarized = rank.median(binarized, disk(5))
if SHOW_IMAGES:
    axes[5].imshow(binarized, cmap=cmap, aspect='auto')

print("Save image")
io.imsave("binarized.png", binarized)

if SHOW_IMAGES:
    fig.tight_layout()
    plt.show()
