#!/usr/bin/env

import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from skimage.morphology import skeletonize, square
from skimage.util import img_as_bool, img_as_ubyte
from skimage.filters import rank
from skimage.color import label2rgb

SHOW_IMAGES = True

if SHOW_IMAGES:
    numFigs = 6
    cols = int(np.ceil(np.sqrt(numFigs)))
    rows = int(np.floor(numFigs/cols))
    fig, axes = plt.subplots(rows, cols, figsize=(4*cols, 4*rows))
    cmap = plt.cm.gray
    axes = axes.flatten()

img = io.imread("binarized.png")
if SHOW_IMAGES:
    axes[0].imshow(img, cmap=cmap, aspect='auto')

skeleton = skeletonize(img_as_bool(img))
if SHOW_IMAGES:
    axes[1].imshow(skeleton, cmap=cmap, aspect='auto')

sk = img_as_ubyte(skeleton)
sk[sk == 255] = 1
summed = rank.sum(img_as_ubyte(sk), square(3)) * sk
axes[3].hist(summed.flatten())
endpoints = (summed > 0) & (summed < 3)
#print(np.min(endpoints.flatten()), np.max(endpoints.flatten()))
#skeleton_labeled = label2rgb(img_as_ubyte(endpoints), image=skeleton)
if SHOW_IMAGES:
    axes[2].imshow(img_as_ubyte(endpoints), aspect='auto')

if SHOW_IMAGES:
    fig.tight_layout()
    plt.show()
