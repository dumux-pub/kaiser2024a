"""
Write graph with data to VTK polydata file
Author: Timo Koch
"""
import lxml.etree as xml

def write_vtp(filename, vertices_array, connectivity_array, point_data, cell_data):
    """Write data to vtp file"""
    numPoints = len(vertices_array)
    numLines = len(connectivity_array)
    vtkTag = xml.Element("VTKFile", {
        "type": "PolyData",
        "version": "0.1",
        "byte_order": "LittleEndian",
        })
    polyDataTag = xml.SubElement(vtkTag, "PolyData")
    piece = xml.SubElement(polyDataTag, "Piece", {
        "NumberOfPoints": str(numPoints),
        "NumberOfVerts": "0",
        "NumberOfLines": str(numLines),
        "NumberOfStrips": "0",
        "NumberOfPolys": "0",
    })

    points = xml.SubElement(piece, "Points")
    lines = xml.SubElement(piece, "Lines")
    if point_data:
        pointData = xml.SubElement(piece, "PointData")
    if cell_data:
        cellData = xml.SubElement(piece, "CellData")

    # point data
    for name, data_array in point_data.items():
        data = xml.SubElement(pointData, "DataArray", {"type":"Float64", "Name":name, "NumberOfComponents":"1", "format":"ascii"})
        data.text = " ".join(["{:10.15e}".format(p) for p in data_array])

    # cell data
    for name, data_array in cell_data.items():
        data = xml.SubElement(cellData, "DataArray", {"type":"Float64", "Name":name, "NumberOfComponents":"1", "format":"ascii"})
        data.text = " ".join(["{:10.15e}".format(r) for r in data_array])

    # points
    coords = xml.SubElement(points, "DataArray", {"type":"Float64", "NumberOfComponents":"3", "format":"ascii"})
    if len(vertices_array[0]) == 2:
        coords.text = " ".join(["{:10.15e}".format(i) for c in vertices_array for i in [*c, 0.0]])
    else:
        assert len(vertices_array[0]) == 3
        coords.text = " ".join(["{:10.15e}".format(i) for c in vertices_array for i in c])

    # lines
    connectivity = xml.SubElement(lines, "DataArray", {"type":"Int32", "Name":"connectivity", "format":"ascii"})
    offsets = xml.SubElement(lines, "DataArray", {"type":"Int32", "Name":"offsets", "format":"ascii"})
    connectivity.text = " ".join(["{}".format(i) for c in connectivity_array for i in c])
    offsets.text = " ".join(["{}".format((i+1)*2) + " " for i in range(len(connectivity_array))])

    with open(filename, 'w') as vtkOutput:
        vtkOutput.write(xml.tostring(vtkTag, pretty_print=True, encoding="unicode"))
