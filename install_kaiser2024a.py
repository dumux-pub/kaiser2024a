#!/usr/bin/env python3

# 
# This installs the module kaiser2024a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |      dune-alugrid     |  origin/releases/2.9  |  0f80e6d29e80dfa7f79a19369ae65199df77587a  |  2024-02-01 14:21:15 +0000  |
# |       dune-grid       |  origin/releases/2.9  |  75b66b0ebf0656e21af08798188b3d2848c9574d  |  2023-12-16 13:50:39 +0000  |
# |      kaiser2024a      |      origin/main      |  1427f72e3e8cff8b40f2da8400b87d13fa223e6b  |  2024-05-11 18:02:24 +0200  |
# |       dune-istl       |  origin/releases/2.9  |  1582b9e200ad098d0f00de2c135f9eed38508319  |  2023-10-19 09:15:16 +0000  |
# |  dune-localfunctions  |  origin/releases/2.9  |  f2c7cfb96327fbfd29744dccf5eac015a1dfa06f  |  2023-12-16 13:51:43 +0000  |
# |         dumux         |     origin/master     |  7e8c2744b2b69db3357390970748c8cd92dc6b21  |  2024-04-26 06:29:51 +0000  |
# |     dune-geometry     |  origin/releases/2.9  |  7d5b1d81ad997f81637ac97f753f80a64ff9cdb0  |  2023-12-16 13:50:03 +0000  |
# |     dune-foamgrid     |  origin/releases/2.9  |  2bf9fe68e6ccaa389adbdbdeff41557b99ed0375  |  2023-07-20 23:55:46 +0000  |
# |      dune-subgrid     |     origin/master     |  41ab447c59ea508c4b965be935b81928e7985a6b  |  2022-09-25 23:18:45 +0000  |
# |      dune-common      |  origin/releases/2.9  |  cce9c197680c1a9964d21ab4d0e0835ea36affb0  |  2024-05-09 14:48:33 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.split("/")[-1]
    if targetFolder.endswith(".git"):
        targetFolder = targetFolder[:-4]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/releases/2.9", "0f80e6d29e80dfa7f79a19369ae65199df77587a", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.9", "75b66b0ebf0656e21af08798188b3d2848c9574d", )

print("Installing kaiser2024a")
installModule("kaiser2024a", "git@git.iws.uni-stuttgart.de:dumux-pub/kaiser2024a.git", "origin/main", "1427f72e3e8cff8b40f2da8400b87d13fa223e6b", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.9", "1582b9e200ad098d0f00de2c135f9eed38508319", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.9", "f2c7cfb96327fbfd29744dccf5eac015a1dfa06f", )

print("Installing dumux")
installModule("dumux", "git@git.iws.uni-stuttgart.de:dumux-repositories/dumux.git", "origin/master", "7e8c2744b2b69db3357390970748c8cd92dc6b21", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.9", "7d5b1d81ad997f81637ac97f753f80a64ff9cdb0", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/releases/2.9", "2bf9fe68e6ccaa389adbdbdeff41557b99ed0375", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid.git", "origin/master", "41ab447c59ea508c4b965be935b81928e7985a6b", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.9", "cce9c197680c1a9964d21ab4d0e0835ea36affb0", )

print("Applying patch for uncommitted changes in kaiser2024a")
patch = """
diff --git a/README.md b/README.md
index f333f7b..b1f4a24 100644
--- a/README.md
+++ b/README.md
@@ -54,3 +54,20 @@ struct viewThreadSafe< SubGrid<dim, HostGrid, MapIndexStorage> > {
 }
 ```
 
+and using a parallel SOR method `dumux/linear/seqsolverbackend.hh`:
+
+replace
+
+```cpp
+    template<class M, class X, std::size_t i>
+    using Smoother = Dune::SeqSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
+```
+
+by
+
+```cpp
+    template<class M, class X, std::size_t i>
+    using Smoother = Dumux::ParMTSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
+```
+
+Both changes will also be applied when using the 
\\ No newline at end of file
"""
applyPatch("kaiser2024a", patch)

print("Applying patch for uncommitted changes in dumux")
patch = """
diff --git a/dumux/linear/seqsolverbackend.hh b/dumux/linear/seqsolverbackend.hh
index 103961b4b7..afdde79c72 100644
--- a/dumux/linear/seqsolverbackend.hh
+++ b/dumux/linear/seqsolverbackend.hh
@@ -525,7 +525,8 @@ class BlockDiagAMGBiCGSTABSolver : public LinearSolver
     using VecBlockType = std::decay_t<decltype(std::declval<X>()[Dune::index_constant<i>{}])>;
 
     template<class M, class X, std::size_t i>
-    using Smoother = Dune::SeqSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
+    // using Smoother = Dune::SeqSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
+    using Smoother = Dumux::ParMTSSOR<DiagBlockType<M, i>, VecBlockType<X, i>, VecBlockType<X, i>>;
 
     template<class M, class X, std::size_t i>
     using SmootherArgs = typename Dune::Amg::SmootherTraits<Smoother<M, X, i>>::Arguments;
"""
applyPatch("dumux", patch)

print("Applying patch for uncommitted changes in dune-subgrid")
patch = """
diff --git a/dune/subgrid/subgrid.hh b/dune/subgrid/subgrid.hh
index 38b5dae..eadf867 100644
--- a/dune/subgrid/subgrid.hh
+++ b/dune/subgrid/subgrid.hh
@@ -1588,6 +1588,12 @@ namespace Capabilities
     {
         static const bool v = true;
     };
+
+    //! \\brief SubGrid is thread-safe for grid views
+    template<int dim, class HostGrid, bool MapIndexStorage>
+    struct viewThreadSafe< SubGrid<dim, HostGrid, MapIndexStorage> > {
+      static const bool v = true;
+    };
 }
 
 template<int dim, class HostGrid, bool MapIndexStorage>
"""
applyPatch("dune-subgrid", patch)

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
