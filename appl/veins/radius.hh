// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief Computing vein radius according to Murray's law
 */
#ifndef DUMUX_VEIN_RADIUS_HH
#define DUMUX_VEIN_RADIUS_HH

#include <vector>
#include <stack>
#include <cmath>
#include <algorithm>

namespace Dumux::LeafVeinGenerator {

// Compute branch radii according to Murray's law
// with a given exponent (r_parent^n = r_1^n + r_2^n + ...)
// The grid is forming a directed graph (by local numbering).
// Vertices with only inflowing elements are the seeds and are assigned the minimum radius
// We then proceed upstream ending in the roots (vertices with only outflowing edges).
template<class GridView>
void computeRadii(const GridView& gridView,
                  std::vector<double>& radius,
                  double minRadius = 0.3, double exponent = 2.3)
{
    using ElementSeed = typename GridView::template Codim<0>::Entity::EntitySeed;
    constexpr int dim = GridView::dimension;

    radius.resize(gridView.size(0));

    std::vector<bool> visited(gridView.size(0), false);
    std::vector<ElementSeed> seeds(gridView.size(0));

    // for each vertex, find all inflowing and outflowing edges
    struct VertexData
    {
        std::vector<std::size_t> in;
        std::vector<std::size_t> out;
    };

    std::vector<VertexData> vertexData(gridView.size(dim));
    for (const auto& element : elements(gridView))
    {
        const auto eIdx = gridView.indexSet().index(element);
        seeds[eIdx] = element.seed();
        for (const auto& is : intersections(gridView, element))
        {
            const auto vIdx = gridView.indexSet().subIndex(element, is.indexInInside(), dim);
            if (is.indexInInside() == 0)
                vertexData[vIdx].out.push_back(eIdx);
            else
                vertexData[vIdx].in.push_back(eIdx);
        }
    }

    // seeds (veinlets) are all elements that have a vertex with only inflow nodes
    std::stack<ElementSeed> elemStack;
    for (const auto& element : elements(gridView))
    {
        for (const auto& is : intersections(gridView, element))
        {
            const auto vIdx = gridView.indexSet().subIndex(element, is.indexInInside(), dim);
            if (vertexData[vIdx].out.empty() && !vertexData[vIdx].in.empty())
            {
                elemStack.push(element.seed());
                break; // make sure to only add each element once
            }
        }
    }

    // the element kernel
    const auto handleElement = [&](const auto& element)
    {
        const auto eIdx = gridView.indexSet().index(element);

        // get the downstream node and compute radius
        // elements are only added if all downstream neighbors are ready
        {
            const auto vIdx = gridView.indexSet().subIndex(element, 1, dim);
            if (vertexData[vIdx].out.empty())
                radius[eIdx] = minRadius;

            else
            {
                // compute radius with Murray's law
                double R = 0.0;
                for (auto childEIdx : vertexData[vIdx].out)
                    R += std::pow(radius[childEIdx], exponent);
                // in case of multiple upstream elements, all upstream get the same radius
                // that means all downstream nodes contribute equally
                R /= vertexData[vIdx].in.size();
                radius[eIdx] = std::pow(R, 1.0/exponent);
            }

            // mark this element as handled
            visited[eIdx] = true;
        }

        // now we check if handling this element completed the
        // downstream element set of the upstream vertex
        // if so, add all upstream elements to the stack
        {
            const auto vIdx = gridView.indexSet().subIndex(element, 0, dim);
            if (!vertexData[vIdx].in.empty()) // not root
            {
                const bool ready = std::all_of(
                    vertexData[vIdx].out.begin(), vertexData[vIdx].out.end(),
                    [&](const auto& e) { return visited[e]; }
                );

                if (ready)
                    for (auto parentEIdx : vertexData[vIdx].in)
                        if (!visited[parentEIdx])
                            elemStack.push(seeds[parentEIdx]);
            }
        }
    };

    while (!elemStack.empty())
    {
        auto seed = elemStack.top();
        elemStack.pop();
        handleElement(gridView.grid().entity(seed));
    }
}

} // end namespace Dumux::LeafVeinGenerator

#endif
