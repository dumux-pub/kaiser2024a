// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geometry
 * \brief Find the k >= 1 closest entities in a spatial data structure
 */
#ifndef DUMUX_GEOMETRY_KCLOSEST_ENTITIES_SPHERE_HH
#define DUMUX_GEOMETRY_KCLOSEST_ENTITIES_SPHERE_HH

namespace Dumux::Detail {

/*!
 * \ingroup Geometry
 * \brief Compute the k closest entities in an AABB tree (index and shortest squared distance) recursively
 * \note specialization for point geometries (point cloud AABB tree)
 * \note the input/output result is a sorted list with k elements
 */
template<class EntitySet, class ctype, int dimworld,
         typename std::enable_if_t<(EntitySet::Entity::Geometry::mydimension == 0), int> = 0>
void kClosestEntities(const Dune::FieldVector<ctype, dimworld>& point,
                      const BoundingBoxTree<EntitySet>& tree,
                      std::size_t node,
                      std::list<std::pair<ctype, std::size_t>>& result)
{
    // Get the bounding box for the current node
    const auto& bBox = tree.getBoundingBoxNode(node);

    // If the bounding box is a leaf, update distance and index with primitive test
    if (tree.isLeaf(bBox, node))
    {
        const std::size_t entityIdx = bBox.child1;
        const auto& p = tree.entitySet().entity(entityIdx).geometry().corner(0);
        const auto squaredDistance = (p-point).two_norm2();

        if (auto it = std::partition_point(
            result.begin(), result.end(), [=](const auto& e){ return e.first <= squaredDistance; }
        ); it != result.end()) // it points to an entity with larger distance
        {
            // we found a closer entity and insert it
            result.insert(it, std::make_pair(squaredDistance, entityIdx));
            result.pop_back(); // remove the largest element
        }
    }

    // Check the children nodes recursively
    else
    {
        // If bounding box is outside radius, then don't search further
        const auto squaredDistance = squaredDistancePointBoundingBox(
            point, tree.getBoundingBoxCoordinates(node)
        );

        // do not continue if the AABB is further away than
        // the current maximum distance in the set of candidate points
        if (squaredDistance > result.back().first) return;

        closestEntity(point, tree, bBox.child0, result);
        closestEntity(point, tree, bBox.child1, result);
    }
}

} // end namespace Dumux::Detail

namespace Dumux {

/*!
 * \ingroup Geometry
 * \brief Compute the closest entity in an AABB tree to a point (index and shortest squared distance)
 * \param point the point
 * \param tree the AABB tree
 * \param minSquaredDistance conservative estimate of the minimum distance
 *
 * \note Returns a list of <= k points and squared distances
 *       The list is only smaller than k if there is less than k elements in the tree
 */
template<class EntitySet, class ctype, int dimworld>
std::list<std::pair<ctype, std::size_t>>
kClosestEntities(const Dune::FieldVector<ctype, dimworld>& point,
                 const BoundingBoxTree<EntitySet>& tree,
                 std::size_t k)
{
    std::list<std::pair<ctype, std::size_t>> result;
    const auto& entitySet = tree.entitySet();
    k = std::min(k, entitySet.size());

    auto eIt = entitySet.begin();
    for (std::size_t i = 0; i < k; ++eIt, ++i)
    {
        const auto squaredDistance = (eIt->geometry().corner(0)-point).two_norm2();
        result.emplace_back(squaredDistance, entitySet.index(*eIt));
    }

    result.sort([](const auto& a, const auto& b){ return a.first < b.first; });

    // we only need to actually search if there is more than k elements in the tree
    if (entitySet.size() > k)
        Detail::kClosestEntities(point, tree, tree.numBoundingBoxes() - 1, result);

    return result;
}

} // end namespace Dumux

#endif
