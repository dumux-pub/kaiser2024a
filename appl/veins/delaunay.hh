// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geometry
 * \brief Delaunay triangulation using CGAL as backend
 */
#ifndef DUMUX_GEOMETRY_DELAUNAY_TRIANGULATION_HH
#define DUMUX_GEOMETRY_DELAUNAY_TRIANGULATION_HH

#include <dumux/common/math.hh>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>

#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <CGAL/IO/write_VTU.h>

namespace Dumux::Detail {

template<class Point>
auto delaunayNeighborGraph(const std::vector<Point>& points)
{
    using CGALKernel = CGAL::Exact_predicates_inexact_constructions_kernel;
    using CGALGeoTraits = CGAL::Projection_traits_xy_3<CGALKernel>;
    using VertexBaseWithIndex = CGAL::Triangulation_vertex_base_with_info_2<std::size_t, CGALGeoTraits>;
    using CGALTriangulationData = CGAL::Triangulation_data_structure_2<VertexBaseWithIndex>;
    using CGALDelaunay2D = CGAL::Delaunay_triangulation_2<CGALGeoTraits, CGALTriangulationData>;
    using CGALPoint = CGALDelaunay2D::Point;

    std::vector<std::pair<CGALPoint, std::size_t>> cgalPoints(points.size());
    for (std::size_t i = 0; i < points.size(); ++i)
        cgalPoints[i] = std::make_pair(CGALPoint(points[i][0], points[i][1], points[i][2]), i);

    CGALDelaunay2D dt(cgalPoints.begin(), cgalPoints.end());

    struct Neighbor { std::size_t index; double squaredDistance; };
    std::vector<std::vector<Neighbor>> neighbors(points.size());

    // iterate over all edges and compute angles and distances
    for (auto eIt = dt.finite_edges_begin(); eIt != dt.finite_edges_end(); ++eIt)
    {
        // see https://stackoverflow.com/questions/9683809/cgal-2d-delaunay-triangulation-how-to-get-edges-as-vertex-id-pairs
        const auto i = eIt->first->vertex( (eIt->second+1)%3 )->info();
        const auto j = eIt->first->vertex( (eIt->second+2)%3 )->info();
        const auto sqDist = (points[i]-points[j]).two_norm2();
        neighbors[i].emplace_back(Neighbor{j, sqDist});
        neighbors[j].emplace_back(Neighbor{i, sqDist});
    }

    return neighbors;
}
} // namespace Dumux::Detail

namespace Dumux::LeafVeinGenerator {

template<class LeafShape>
void meshLeafShape(const LeafShape& leafShape, const std::string& fileName, const double meshSize)
{
    using CGALKernel = CGAL::Exact_predicates_inexact_constructions_kernel;
    using VB = CGAL::Triangulation_vertex_base_2<CGALKernel>;
    using FB = CGAL::Delaunay_mesh_face_base_2<CGALKernel>;
    using TriangulationData = CGAL::Triangulation_data_structure_2<VB, FB>;
    using CGALDelaunay2D = CGAL::Constrained_Delaunay_triangulation_2<CGALKernel, TriangulationData>;
    using CGALPoint = CGALDelaunay2D::Point;
    using VertexHandle = CGALDelaunay2D::Vertex_handle;
    using Criteria = CGAL::Delaunay_mesh_size_criteria_2<CGALDelaunay2D>;


    const std::size_t samples = 100;
    const auto heights = linspace(0.0, leafShape.bBoxMax()[1], samples, /*endPoint=*/true);
    std::vector<CGALPoint> cgalPoints(samples*2 - 2);
    for (std::size_t i = 0; i < samples; ++i)
        cgalPoints[i] = CGALPoint(leafShape.width(heights[i]), heights[i]);
    for (std::size_t i = 1; i < samples - 1; ++i)
        cgalPoints[samples + i - 1] = CGALPoint(-leafShape.width(heights[i]), heights[i]);
    std::reverse(cgalPoints.begin() + samples, cgalPoints.end());

    // make constrained delaunay triangulation
    CGALDelaunay2D cdt;

    // add outline as constraint
    const VertexHandle v0 = cdt.insert(cgalPoints[0]);
    VertexHandle vPrev = v0;
    for (std::size_t i = 1; i < samples*2 - 2; ++i)
    {
        VertexHandle vNext = cdt.insert(cgalPoints[i]);
        cdt.insert_constraint(vPrev, vNext);
        vPrev = vNext;
    }
    cdt.insert_constraint(vPrev, v0);

    CGAL::refine_Delaunay_mesh_2(cdt, Criteria(0.125, meshSize));
    std::ofstream vtuFile(fileName);
    CGAL::write_vtu(vtuFile, cdt);

    std::cout << Fmt::format("🌿 Created leaf mesh with {} vertices ({})\n", cdt.number_of_vertices(), fileName);
}

} // end namespace Dumux::LeafVeinGenerator

#endif
