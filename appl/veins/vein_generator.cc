#include <config.h>

#include "radius.hh"
#include "veingraph.hh"
#include "leafshape.hh"
#include "relativeneighborhoodgraph.hh"
#include "forces.hh"
#include "delaunay.hh"
#include "grow.hh"
#include "attractor.hh"
#include "delaunay.hh"

#include <iostream>
#include <random>
#include <algorithm>
#include <vector>
#include <array>
#include <unordered_set>

#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/initialize.hh>
#include <dumux/io/format.hh>
#include <dumux/geometry/distance.hh>

#include <test/geometry/writetriangulation.hh>

/* \brief Leaf vein patterns
 *
 * This implementation follows the paper "Modeling and visualization of leaf venation patterns"
 * by A. Runions, M. Fuhrer, B. Lane, P. Federl, A. Rolland-Lagan, P. Prusinkiewicz
 * published 2005 in ACM Transactions on Graphics https://doi.org/10.1145/1073204.1073251
 */
int main (int argc, char *argv[])
{
    using namespace Dumux;

    // maybe initialize mpi / multi-threading environment
    initialize(argc, argv);

    // read input file
    Parameters::init(argc, argv);

    const double finalLeafHeight = getParam<double>("LeafShape.ApexHeight");
    const double initialLeafScale = getParam<double>("LeafGrowth.InitialLeafHeight")/finalLeafHeight;
    const double leafGrowthRate = getParam<double>("LeafGrowth.GrowthRatePerStep")/finalLeafHeight;
    // switch from marginal growth to uniform growth
    const bool uniformGrowth = getParam<bool>("UniformGrowth", false);
    // whether we seek an open (no loops) or a closed (anastomoses) vein pattern
    const bool openPattern = getParam<bool>("OpenPattern", false);
    const double initialStepSize = getParam<double>("StepSize", 1.0);
    const double initialSegmentLength = getParam<double>("InitialSegmentLength", 0.1);

    LeafVeinGenerator::LeafShape<3> leafShape;
    leafShape.setScale(initialLeafScale);

    LeafVeinGenerator::VeinGraph<3> graph(initialSegmentLength);

    const double attractorsPerAreaPerStep = getParam<double>("AttractorsPerAreaPerStep", 100e-6);
    const double initialBirthDist = getParam<double>("BirthDist");
    LeafVeinGenerator::Attractor<3> attractor;
    attractor.spawn(attractorsPerAreaPerStep, leafShape, graph, initialBirthDist);

    using GV = LeafVeinGenerator::VeinGraph<3>::Grid::LeafGridView;
    const GV gridView = graph.gridView();

    Dune::VTKSequenceWriter<GV> vtkWriter(gridView, "veins", "", "");
    const double minRadius = getParam<double>("Radius.MinRadius", 0.001);
    const double tubeLawExponent = getParam<double>("Radius.TubeLawExponent", 2.3);
    std::vector<double> radius(graph.size(), minRadius);
    vtkWriter.addCellData(radius, "radius");

    const std::size_t maxIterations = getParam<std::size_t>("MaxIterations", 1000);
    const std::size_t maxNumSegments = getParam<std::size_t>("MaxSegments", 10000);
    const std::size_t outputInterval = getParam<std::size_t>("OutputInterval", 5);

    const double initialKillDist = getParam<double>("KillDist");
    const double neighborHoodGraphMaxDistance
        = getParam<double>("NeighborhoodGraph.MaxDist", std::sqrt(std::numeric_limits<double>::max()));
    const double neighborHoodGraphMaxDistanceSquared = neighborHoodGraphMaxDistance*neighborHoodGraphMaxDistance;

    std::size_t numSegments = graph.size();
    std::size_t outputCount = 0;
    std::size_t iteration = 0;

    double birthDist = initialBirthDist;
    double killDist = initialKillDist;
    double stepSize = initialStepSize;
    double leafScaleUniformGrowth = initialLeafScale;
    double initialLeafScaleUniformGrowth = initialLeafScale;

    Dune::Timer timer;
    while (numSegments < maxNumSegments && iteration < maxIterations)
    {
        // LeafVeinGenerator::meshLeafShape(leafShape, "leaf_0.vtu", initialStepSize);
        std::cout << std::endl;

        // timings (reported at the end of each loop)
        double forcesTime = 0.0, growTime = 0.0, pruneTime = 0.0, neighborTime = 0.0, spawnTime = 0.0, outputTime = 0.0;
        timer.reset();

        // From https://doi.org/10.1145/1073204.1073251:
        // "For computational efficiency, instead of scaling up the leaf,
        // we scale down the unit distance λ used
        // while inserting [birthDist] and removing [killDist] auxin sources: λt = λ0 · σ0 /σt"
        // [where σ is the scaling factor]
        if (uniformGrowth)
        {
            birthDist = initialBirthDist * initialLeafScaleUniformGrowth/leafScaleUniformGrowth;
            killDist = std::max(initialKillDist * initialLeafScaleUniformGrowth/leafScaleUniformGrowth, stepSize*1.1);
        }

        // open patterns (tips stay separate)
        if (openPattern)
        {
            std::cout << Fmt::format("🌿 [#{}] Open pattern algorithm\n", iteration);

            const auto forces = LeafVeinGenerator::computeForces(graph, attractor, std::numeric_limits<double>::max());
            forcesTime += timer.elapsed(); timer.reset();

            // grow the vein graph using the computed forces and a step size
            LeafVeinGenerator::grow(graph, forces, stepSize);
            growTime += timer.elapsed(); timer.reset();

            // debug vtk output (attractor points and neighborhood graph)
            if (iteration % outputInterval == 0)
            {
                attractor.dumpPointsToFile(Fmt::format("points-{}.txt", outputCount));
                LeafVeinGenerator::computeRadii(gridView, radius, minRadius, tubeLawExponent);
                vtkWriter.write(outputCount);
                outputCount++;

                outputTime += timer.elapsed(); timer.reset();
            }

            // prune attractors
            attractor.prune(graph, killDist);
            pruneTime += timer.elapsed(); timer.reset();
        }

        // closed pattern (anastomoses)
        else
        {
            std::cout << Fmt::format("🌿 [#{}] Closed pattern algorithm\n", iteration);

            // compute relative neighborhood graph
            auto neighborGraph = relativeNeighborhoodGraph(attractor, graph, neighborHoodGraphMaxDistanceSquared);
            neighborTime += timer.elapsed(); timer.reset();

            // prune attractors
            LeafVeinGenerator::mergeAndPrune(graph, attractor, neighborGraph, 0.5*stepSize, killDist);
            pruneTime += timer.elapsed(); timer.reset();

            // recompute the neighbor graph after mergers and pruning (that invalidates the graph)
            neighborGraph = relativeNeighborhoodGraph(attractor, graph, neighborHoodGraphMaxDistanceSquared);
            neighborTime += timer.elapsed(); timer.reset();

            // debug vtk output (attractor points and neighborhood graph)
            if (iteration % outputInterval == 0)
            {
                std::vector<std::array<typename LeafVeinGenerator::VeinGraph<3>::Point, 2>> lines;
                for (std::size_t i = 0; i < neighborGraph.size(); ++i)
                    for (const auto& neighbor : neighborGraph[i])
                        lines.push_back({{ attractor.points()[i], graph.vertexSet().entity(neighbor.index).geometry().corner(0) }});

                writeVTKPolyDataLines(lines, Fmt::format("neighborhood-{}", outputCount));
                attractor.dumpPointsToFile(Fmt::format("points-{}.txt", outputCount));
                LeafVeinGenerator::computeRadii(gridView, radius, minRadius, tubeLawExponent);
                vtkWriter.write(outputCount);
                outputCount++;

                outputTime += timer.elapsed(); timer.reset();
            }

            timer.reset();
            const auto forces = LeafVeinGenerator::computeForces(graph, attractor, neighborGraph);
            forcesTime += timer.elapsed(); timer.reset();

            // grow the vein graph using the computed forces and a step size
            LeafVeinGenerator::grow(graph, forces, stepSize);
            growTime += timer.elapsed(); timer.reset();

            // debug vtk output (attractor points and neighborhood graph)
            if (iteration % outputInterval == 0)
            {
                std::vector<std::array<typename LeafVeinGenerator::VeinGraph<3>::Point, 2>> lines;
                for (std::size_t i = 0; i < neighborGraph.size(); ++i)
                    for (const auto& neighbor : neighborGraph[i])
                        lines.push_back({{ attractor.points()[i], graph.vertexSet().entity(neighbor.index).geometry().corner(0) }});

                writeVTKPolyDataLines(lines, Fmt::format("neighborhood-{}", outputCount));
                attractor.dumpPointsToFile(Fmt::format("points-{}.txt", outputCount));
                LeafVeinGenerator::computeRadii(gridView, radius, minRadius, tubeLawExponent);
                vtkWriter.write(outputCount);
                outputCount++;

                outputTime += timer.elapsed(); timer.reset();
            }
        }

        // update num segments
        numSegments = graph.size();

        // output iteration statistics
        std::cout << Fmt::format(
            "Status for step {}: step size: {:.3e}, kill distance: {:.3e}, birth distance: {:.3e}, attractors: {}, segments: {}, scale: {:.3e})\n",
            iteration, stepSize, killDist, birthDist, attractor.size(), numSegments, leafShape.scale()
        );

        // this models "uniform growth" (e.g. scaling of the entire leaf)
        if (uniformGrowth)
            leafScaleUniformGrowth = std::min(leafScaleUniformGrowth + leafGrowthRate, 1.0);

        // this models "marginal growth" (e.g. scaling of the leaf outline)
        else
            leafShape.setScale(std::min(leafShape.scale() + leafGrowthRate, 1.0));

        // spawn new attractor points for the next round
        attractor.spawn(attractorsPerAreaPerStep, leafShape, graph, birthDist);
        spawnTime += timer.elapsed(); timer.reset();

        // output timings of this iteration loop step
        std::cout << Fmt::format(
            "Timings for step {}: pruning {:.3e}, forces {:.3e}, grow {:.3e}, output {:.3e}, spawn {:.3e}\n",
            iteration, pruneTime, forcesTime, growTime, outputTime, spawnTime
        );
        if (iteration % outputInterval == 0) {
            LeafVeinGenerator::meshLeafShape(leafShape, Fmt::format("leaf_{}.vtu", outputCount), initialStepSize);
        }
        ++iteration;
    }

    // Calculate the leaf area
    double leafArea = 0.0;
    const int numPoints = 1000;
    const double dy = (leafShape.bBoxMax()[1] - leafShape.bBoxMin()[1]) / numPoints;
    for (int i = 0; i < numPoints; ++i)
    {
        const double y = leafShape.bBoxMin()[1] + i * dy;
        const double halfwidth = leafShape.width(y);
        leafArea += halfwidth * dy * 2.0;
    }

    // Calculate the vein length
    double veinLength = 0.0;
    for (const auto& element : elements(gridView))
    {
        veinLength += element.geometry().volume();
    }

    // Print the leaf area and vein length
    std::cout << Fmt::format("Leaf area: {:.3e}, Vein length: {:.3e}\n", leafArea, veinLength);
    // Print the vein density
    std::cout << Fmt::format("Vein density: {:.3e}\n", veinLength/leafArea);

    return 0;
}
