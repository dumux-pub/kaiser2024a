// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief Data structure representing the vein graph (the grid)
 */
#ifndef DUMUX_VEIN_GRAPH_HH
#define DUMUX_VEIN_GRAPH_HH

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/geometry/boundingboxtree.hh>
#include <dumux/geometry/geometricentityset.hh>

namespace Dumux::LeafVeinGenerator {

template<int dimworld>
class VeinGraph
{
public:
    using Grid = Dune::FoamGrid<1, dimworld>;
    using Point = Dune::FieldVector<double, dimworld>;
    static constexpr int dimensionworld = dimworld;
    static constexpr int dimension = 1;

    using GridView = typename Grid::LeafGridView;
    using VertexSet = GridViewGeometricEntitySet<GridView, GridView::dimension>;
    using BBoxTree = BoundingBoxTree<VertexSet>;

    // initialize the vein graph
    // we always start with the petiole (0,-initialSegmentLength)-->(0,0)
    VeinGraph(double initialSegmentLength)
    {
        Dune::GridFactory<Grid> factory;

        Point p1(0.0);
        Point dir(0.0);
        dir[1] = -1.0;
        Point p0 = p1; p0.axpy(initialSegmentLength, dir);
        factory.insertVertex(p0);
        factory.insertVertex(p1);

        factory.insertElement(
            Dune::GeometryTypes::line,
            std::vector<unsigned int>({0, 1})
        );

        grid_ = std::unique_ptr<Grid>(factory.createGrid());
        update();
    }

    // update state after growing grid
    void update()
    {
        vertexSet_ = std::make_shared<VertexSet>(grid_->leafGridView());
        tree_ = std::make_shared<BBoxTree>(vertexSet_);
    }

    // get a grid view
    typename Grid::LeafGridView gridView() const
    { return grid_->leafGridView(); }

    // number of segments in the graph
    std::size_t size() const
    { return grid_->leafGridView().size(0); }

    // get access to the grid
    Grid& grid()
    { return *grid_; }

    // get access to the vertex set
    const VertexSet& vertexSet() const
    { return *vertexSet_; }

    // get access to the bounding box tree set
    // for fast shortest distance queries
    const BBoxTree& boundingBoxTree() const
    { return *tree_; }

private:
    std::shared_ptr<VertexSet> vertexSet_;
    std::shared_ptr<BBoxTree> tree_;
    std::unique_ptr<Grid> grid_;
};

} // end namespace Dumux::LeafVeinGenerator

#endif
