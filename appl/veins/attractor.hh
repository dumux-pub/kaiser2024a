// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief Algorithm to compute the forces that pull veins towards the attractor
 */
#ifndef DUMUX_VEIN_ATTRACTOR_POINTS_HH
#define DUMUX_VEIN_ATTRACTOR_POINTS_HH

#include "utility.hh"

#include <dune/common/fvector.hh>

#include <random>
#include <vector>
#include <algorithm>

namespace Dumux::LeafVeinGenerator {

template<int dimworld>
class Attractor
{
    using Point = Dune::FieldVector<double, dimworld>;
public:

    // spawn new attractors
    // randomly pick a specified number of points and keep them if they satisfy the insertion criteria
    // no closer than birthDist to any point in the vein graph
    // and no closer than birthDist to any other attractor
    template<class LeafShape, class Graph>
    void spawn(const double attractorsPerArea, const LeafShape& leafShape, const Graph& graph, const double birthDist)
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        const auto bBoxMin = leafShape.bBoxMin();
        const auto bBoxMax = leafShape.bBoxMax();
        std::array<std::uniform_real_distribution<double>, 2> dist{{
            std::uniform_real_distribution<double>(bBoxMin[0], bBoxMax[0]),
            std::uniform_real_distribution<double>(bBoxMin[1], bBoxMax[1])
        }};

        const auto length = bBoxMax-bBoxMin;
        const double area = length[0]*length[1];
        const std::size_t numAttractors = static_cast<std::size_t>(std::ceil(attractorsPerArea*area));
        const auto beforeSize = points_.size();
        points_.reserve(points_.size() + numAttractors);

        const auto& tree = graph.boundingBoxTree();
        const auto birthDistSquared = birthDist*birthDist;
        for (int i = 0; i < numAttractors; ++i)
        {
            // generate point in shape
            Point p(0.0);
            do {
                for (int dimIdx = 0; dimIdx < 2; ++dimIdx)
                    p[dimIdx] = dist[dimIdx](gen);
            } while (!leafShape.contains(p));

            // add point if it satisfied insertion criteria
            const auto sq = squaredDistance(p, tree);
            if (sq > birthDistSquared
                && std::find_if(
                    points_.begin(), points_.end(),
                    [&](const auto& pp){ return (p - pp).two_norm2() < birthDistSquared; }
                ) == points_.end()
            ) points_.emplace_back(std::move(p));
        }
        std::cout << Fmt::format("Created {} new attractor points\n", points_.size()-beforeSize);
    }

    // Default algorithm (usually used in open patterns)
    // kill all attractors that are closer to the veins than the kill distance
    template<class Graph>
    void prune(const Graph& graph, double killDist)
    {
        const auto killDistSquared = killDist*killDist;
        const auto& tree = graph.boundingBoxTree();

        // remove all points that are closer than the kill distance to any node in the graph
        points_.erase(std::remove_if(
            points_.begin(), points_.end(),
            [&](const auto& point){ return squaredDistance(point, tree) < killDistSquared; }
        ), points_.end());
    }

    // Remove all entries for which the mask is true
    // allows for custom pruning algorithms
    void prune(const std::vector<bool>& mask)
    { LeafVeinGenerator::prune(points_, mask); }

    // check if the attractor is empty
    bool empty() const
    { return points_.empty(); }

    // current number of attractor points
    std::size_t size() const
    { return points_.size(); }

    // print for debugging or visualization
    void dumpPointsToFile(const std::string& fileName)
    {
        std::ofstream outfile(fileName, std::ios::out);
        outfile << "x y z\n";
        std::ostream_iterator<Point> it(outfile, "\n");
        std::copy(points_.begin(), points_.end(), it);
    }

    // access the current attractor points
    const std::vector<Point>& points() const
    { return points_; }

private:
    std::vector<Point> points_;
};

} // end namespace Dumux::LeafVeinGenerator

#endif
