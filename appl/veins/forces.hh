// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief Algorithm to compute the forces that pull veins towards the attractor
 */
#ifndef DUMUX_VEIN_FORCES_HH
#define DUMUX_VEIN_FORCES_HH

#include <dune/common/fvector.hh>

namespace Dumux::LeafVeinGenerator {

template<int dimworld>
struct ForceInfo
{
    Dune::FieldVector<double, dimworld> direction{};
    double maxDistance = std::numeric_limits<double>::max();
    std::size_t count = 0;
};

template<class Graph, class Attractor>
std::vector<ForceInfo<Graph::dimensionworld>> computeForces(const Graph& graph, const Attractor& attractor, double maxDist)
{
    const auto& tree = graph.boundingBoxTree();
    const auto& gridView = graph.gridView();
    std::vector<ForceInfo<Graph::dimensionworld>> attractorForces(gridView.size(Graph::dimension));
    for (const auto& point : attractor.points())
    {
        const auto [closestVertexDistSquared, closestVertex] = closestEntity(point, tree);
        if (closestVertexDistSquared > maxDist*maxDist)
            continue;

        // if we found a closest vertex that is not the root
        if (closestVertex != 0)
        {
            const auto direction = point - graph.vertexSet().entity(closestVertex).geometry().corner(0);
            auto& info = attractorForces[closestVertex];
            const auto distanceToAttractor = direction.two_norm();
            info.direction.axpy(1.0/distanceToAttractor, direction);
            info.maxDistance = std::min(info.maxDistance, distanceToAttractor);
            // if this addition results in zero force increase weight by 1%
            if (info.direction.two_norm() < 1e-7)
                info.direction.axpy(0.01/distanceToAttractor, direction);
            info.count++;
        }
    }

    // normalize attractor force
    for (const auto& vertex : vertices(gridView))
    {
        auto& info = attractorForces[graph.vertexSet().index(vertex)];
        if (info.count > 1)
        {
            // normalize direction by count
            info.direction /= static_cast<double>(info.count);
            info.direction /= info.direction.two_norm();
            // reset max distance (we only respect that if there is a single attractor)
            info.maxDistance = std::numeric_limits<double>::max();
        }
    }

    return attractorForces;
}

template<class Graph, class Attractor, class Neighbor>
std::vector<ForceInfo<Graph::dimensionworld>> computeForces(const Graph& graph, const Attractor& attractor, const std::vector<Neighbor>& neighborHood)
{
    const auto& gridView = graph.gridView();
    std::vector<ForceInfo<Graph::dimensionworld>> attractorForces(gridView.size(Graph::dimension));
    if (attractor.size() != neighborHood.size())
        DUNE_THROW(Dune::InvalidStateException, "Wrong size of neighborhood graph!");

    for (std::size_t i = 0; i < attractor.size(); ++i)
    {
        // this point exerts a force on all vertices in the neighborhood
        auto& point = attractor.points()[i];
        for (const auto& neighbor : neighborHood[i])
        {
            const auto direction = point - graph.vertexSet().entity(neighbor.index).geometry().corner(0);
            auto& info = attractorForces[neighbor.index];
            const auto distanceToAttractor = direction.two_norm();
            info.direction.axpy(1.0/distanceToAttractor, direction);
            info.maxDistance = std::min(info.maxDistance, distanceToAttractor);
            info.count++;

            // if this addition results in zero force count this one twice to break the tie
            if (info.direction.two_norm2() < 1e-7)
            {
                info.direction.axpy(1.0/distanceToAttractor, direction);
                info.count++;
            }
        }
    }

    // normalize attractor force
    for (const auto& vertex : vertices(gridView))
    {
        auto& info = attractorForces[graph.vertexSet().index(vertex)];
        if (info.count >= 1)
        {
            // normalize direction by count
            info.direction /= static_cast<double>(info.count);
            info.direction /= info.direction.two_norm();

            // reset max distance (we only respect that if there is a single attractor)
            if (info.count > 1)
                info.maxDistance = std::numeric_limits<double>::max();
        }
    }

    return attractorForces;
}

} // end namespace Dumux::LeafVeinGenerator

#endif
