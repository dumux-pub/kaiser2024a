// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief Utility classes and algorithms
 */
#ifndef DUMUX_VEIN_UTILITY_HH
#define DUMUX_VEIN_UTILITY_HH

#include <vector>

namespace Dumux::LeafVeinGenerator {

// Remove all entries of a vector for which the mask is true
// the vector size is reduces, the order is preserved
template<class T>
void prune(std::vector<T>& p, const std::vector<bool>& mask)
{
    std::size_t j = 0;
    for (std::size_t i = 0; i < mask.size(); ++i)
        if (i >= j && !mask[i])
            p[j++] = std::move(p[i]);
    p.resize(j);
}

} // end namespace Dumux::LeafVeinGenerator

#endif
