// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief Approximation of the local relative neighborhood graph
 *
 * Two points are in relative neighborhood if no third point is
 * closer to both points than they are to each other.
 */
#ifndef DUMUX_VEIN_RELATIVE_NEIGHBORHOOD_GRAPH_HH
#define DUMUX_VEIN_RELATIVE_NEIGHBORHOOD_GRAPH_HH

#include <vector>
#include <algorithm>

#include <dune/common/timer.hh>

#include <dumux/io/format.hh>

namespace Dumux::LeafVeinGenerator {

// compute the relative neighborhood graph of all attractor points and vein graph points
// using the default for maxDist includes all attractor points
// using a smaller value only considers attractor points which are closer to the current vein graph
// than the given distance
template<class Attractor, class Graph>
auto relativeNeighborhoodGraph(const Attractor& attractor,
                               const Graph& graph,
                               const double maxDistSquared = std::numeric_limits<double>::max())
{
    Dune::Timer timer;

    const auto& attractorPoints = attractor.points();
    const auto gridView = graph.gridView();
    std::vector<typename Graph::Point> veinPoints(gridView.size(Graph::dimension));
    for (const auto& vertex : vertices(gridView))
        veinPoints[graph.vertexSet().index(vertex)] = vertex.geometry().corner(0);

    struct Neighbor { std::size_t index; double squaredDistance; double angle; };
    std::vector<std::vector<Neighbor>> relativeNeighborhoodGraph(attractorPoints.size());

    // find the local candidates (distance closer than maxDist)
    // and compute index, distance and angle
    for (std::size_t j = 0; j < veinPoints.size(); ++j)
    {
        for (std::size_t i = 0; i < attractorPoints.size(); ++i)
        {
            const auto distVec = veinPoints[j] - attractorPoints[i];
            const double dist2 = distVec.two_norm2();
            if (dist2 < maxDistSquared)
                relativeNeighborhoodGraph[i].emplace_back(
                    Neighbor{ j, dist2, std::atan2(distVec[0], distVec[1]) }
                );
        }
    }

    // eliminate the edges which are not in the RNG
    // algorithm as described in "Relative Neighborhood Graphs and Their Relatives"
    // Jerzy W. Jaromczyk and Godfried T. Toussaint (Proceedings of the IEEE, 1992)
    // doi: https://doi.org/10.1109/5.163414 (page 13f bottom)
    #pragma omp parallel for
    for (std::size_t i = 0; i < attractorPoints.size(); ++i)
    {
        auto& candidates = relativeNeighborhoodGraph[i];
        if (candidates.size() <= 1)
            continue;

        // sort according to distance
        std::sort(
            candidates.begin(), candidates.end(),
            [&](const auto& a, const auto& b){
                return a.squaredDistance < b.squaredDistance;
            }
        );

        // remove all candidates which are further away and have an angle less than 60
        auto it = candidates.begin();
        auto endIt = candidates.end();

        while (it != endIt)
        {
            endIt = std::remove_if(
                it + 1, endIt,
                [&](const auto& e){
                    return e.squaredDistance > it->squaredDistance
                        && std::min(std::abs(e.angle - it->angle), 2*M_PI - std::abs(e.angle - it->angle))
                            < M_PI*0.333333;
                }
            );

            ++it;
        }

        endIt = std::remove_if(
            candidates.begin(), endIt,
            [&](const auto& e){
                return endIt != std::find_if(
                    candidates.begin(), endIt,
                    [&](const auto& w){
                        return w.index != e.index
                            && w.squaredDistance < e.squaredDistance
                            && std::min(std::abs(w.angle - e.angle), 2*M_PI - std::abs(w.angle - e.angle))
                                < std::acos(0.5*std::sqrt(w.squaredDistance/e.squaredDistance));
                    }
                );
            }
        );

        candidates.erase(endIt, candidates.end());
    }

    std::cout << Fmt::format("Constructed relative neighborhood graph in {} seconds\n", timer.elapsed());
    return relativeNeighborhoodGraph;
}

} // end namespace Dumux::LeafVeinGenerator

#endif
