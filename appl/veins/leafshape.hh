// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief A class representing the leaf geometry (outline)
 */
#ifndef DUMUX_LEAF_SHAPE_HH
#define DUMUX_LEAF_SHAPE_HH

#include <dune/common/fvector.hh>
#include <dumux/common/parameters.hh>

namespace Dumux::LeafVeinGenerator {

// Parameterization as suggested e.g.
// by Shi et al (2021) https://doi.org/10.3390/sym13081524
template<int dimworld>
class LeafShape
{
    using Point = Dune::FieldVector<double, dimworld>;

public:
    LeafShape()
    : maxWidth_(getParam<double>("LeafShape.MaxWidth", 0.25))
    , maxWidthHeight_(getParam<double>("LeafShape.MaxWidthHeight", 0.25))
    , apexHeight_(getParam<double>("LeafShape.ApexHeight", 1.0))
    , shapeParam_(getParam<double>("LeafShape.ShapeFactor", 0.8))
    , scale_(1.0)
    {
        updateBBox_();
    }

    // returns true if the point is contained in the leaf
    bool contains(const Point& p) const
    {
        const auto height = p[1];
        const auto apexHeight = apexHeight_*scale_;
        if (height > apexHeight)
            return false;

        if (const auto w = width(height); w < 1e-8)
            return false;
        else
            return p[0] < w && p[0] > -w;
    }

    double width(double height) const
    {
        const auto apexHeight = apexHeight_*scale_;
        const auto maxWidthHeight = maxWidthHeight_*scale_;
        using std::pow;
        return maxWidth_*scale_*pow(
            height*(height - apexHeight)*(height - apexHeight) / (
                (apexHeight - maxWidthHeight)*(
                    (apexHeight - maxWidthHeight)*(height - maxWidthHeight)
                    + maxWidthHeight*(maxWidthHeight + apexHeight - 2*height)
                )
            )
        , shapeParam_);
    }

    // lower left of axis-aligned bounding box
    const Point& bBoxMin() const
    { return bBoxMin_; }

    // upper right of axis-aligned bounding box
    const Point& bBoxMax() const
    { return bBoxMax_; }

    // set a scale
    void setScale(double s)
    { scale_ = s; updateBBox_(); }

    // get the scale
    double scale() const
    { return scale_; }

private:
    void updateBBox_()
    {
        bBoxMin_ = bBoxMax_ = Point(0.0);
        bBoxMin_[0] = -maxWidth_*scale_;
        bBoxMax_[0] = maxWidth_*scale_;
        bBoxMax_[1] = apexHeight_*scale_;
    }

    double maxWidth_, maxWidthHeight_, apexHeight_, shapeParam_, scale_;
    Point bBoxMin_, bBoxMax_;
};

} // end namespace Dumux::LeafVeinGenerator

#endif
