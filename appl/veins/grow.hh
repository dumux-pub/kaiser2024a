// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup LeafVeinGenerator
 * \brief The grow algorithm (actually growing the vein graph using force field)
 */
#ifndef DUMUX_VEIN_GROW_HH
#define DUMUX_VEIN_GROW_HH

#include "utility.hh"
#include "intersectingentitiessphere.hh"

namespace Dumux::LeafVeinGenerator {

/*
 * \brief Grow the venation pattern with a given force field (growth direction and step size)
 * \note We implement a distance and an angle criterion which might not allow all candidates to grow
 */
template<class VeinGraph, class Force>
void grow(VeinGraph& graph, const std::vector<Force>& forces, double stepSize)
{
    graph.grid().preGrow();
    const auto gridView = graph.gridView();

    // pre-compute normalized orientations of all elements
    std::vector<typename VeinGraph::Point> orientations(gridView.size(0));
    for (const auto& element : elements(gridView))
    {
        const auto eIdx = gridView.indexSet().index(element);
        const auto geo = element.geometry();
        orientations[eIdx] = geo.corner(1) - geo.corner(0);
        orientations[eIdx] /= orientations[eIdx].two_norm();
    }

    constexpr int dim = VeinGraph::dimension;
    for (const auto& element : elements(gridView))
    {
        // for each element we are interested in the forward node
        const unsigned int vIdxGlobal = gridView.indexSet().subIndex(element, 1, dim);
        if (forces[vIdxGlobal].count > 0)
        {
            // distance criterion: if we are too close to some node (maxDistance)
            // we can't do a full step. In this case, we skip the step if it's too small
            const auto size = std::min(stepSize, forces[vIdxGlobal].maxDistance - 0.05*stepSize);
            if (size < 0.5*stepSize)
                continue;

            // angle criterion: each outflowing neighbor blocks an angle defined by
            // it's growth direction vector. We shouldn't grow into this angle
            // otherwise there is a chance of growing too elements on top of each other.
            const auto& growthDirection = forces[vIdxGlobal].direction;
            const bool angleIsAdmissible = [&]()
            {
                // don't make sharper turns than 90° in one step (cos(90°) = 0.0)
                const auto eIdx = gridView.indexSet().index(element);
                if (growthDirection * orientations[eIdx] < 0.0)
                    return false;

                // downstream neighbors
                for (const auto& is : intersections(gridView, element))
                {
                    // this means there is a downstream neighbor node
                    if (is.indexInInside() == 1 && is.neighbor())
                    {
                        // exclude if closer than 60° to downstream neighbor orientation (cos(60°) = 0.5)
                        const auto nIdx = gridView.indexSet().index(is.outside());
                        if (growthDirection * orientations[nIdx] > 0.5)
                            return false;
                    }
                }

                return true;
            }();

            if (angleIsAdmissible)
            {
                // grow
                auto pos = element.geometry().corner(1);
                pos.axpy(size, forces[vIdxGlobal].direction);
                const auto newVIdx = graph.grid().insertVertex(pos);
                graph.grid().insertElement(
                    Dune::GeometryTypes::line,
                    std::vector<unsigned int>({ vIdxGlobal, newVIdx })
                );
            }
        }
    }

    const bool newElements = graph.grid().grow();
    graph.grid().postGrow();

    if (newElements)
        graph.update();
}

/*
 * \brief Given veins and attractors merge all vein vertices within
 *        the merge distance into the attractor point and prune the attractor point
 *        Only tips will be merged (everything else can become quite complicated)
 *
 * \note This step invalidates the neighbor graph logically
 * \note This step removes attractor point involved in mergers
 */
template<class VeinGraph, class Attractor, class Neighbors>
void mergeAndPrune(VeinGraph& graph,
                   Attractor& attractor,
                   const std::vector<Neighbors>& neighborGraph,
                   const double mergeDistance,
                   const double killDist)
{
    const auto mergeDistSquared = mergeDistance*mergeDistance;
    const auto killDistSquared = killDist*killDist;

    const auto& gridView = graph.gridView();
    constexpr int dim = VeinGraph::dimension;

    // whether a vertex is a tip
    std::vector<bool> isTip(gridView.size(dim), false);
    // for all vertices the indices of the neighboring vertices
    std::vector<std::vector<std::size_t>> veinNeighborGraph(gridView.size(dim));
    for (const auto& element : elements(gridView))
    {
        // neighboring vertices
        const auto v0Idx = gridView.indexSet().subIndex(element, 0, dim);
        const auto v1Idx = gridView.indexSet().subIndex(element, 1, dim);
        veinNeighborGraph[v0Idx].push_back(v1Idx);
        veinNeighborGraph[v1Idx].push_back(v0Idx);

        // tip map
        for (const auto& is : intersections(gridView, element))
            if (is.boundary() && is.indexInInside() == 1)
                isTip[v1Idx] = true;
    }

    // mask attractor points to be removed
    const auto& attractorPoints = attractor.points();
    std::vector<bool> mask(attractorPoints.size(), false);

    // maps from current vertex index to new merger point
    std::vector<int> mapVertexToMergerPoint(gridView.size(dim), -1);

    // reserve some buffer memory and other variables for merge algorithm
    std::vector<std::size_t> mergeCandidates; mergeCandidates.reserve(10);
    std::vector<typename VeinGraph::Point> newPoints;

    for (std::size_t i = 0; i < attractorPoints.size(); ++i)
    {
        const auto& neighbors = neighborGraph[i];
        const auto numNeighbors = neighbors.size();
        // if the attractor doesn't attract any point we are done
        if (numNeighbors == 0)
            continue;

        // if this attractor only has a single neighbor remove it if closer than kill distance
        // this is the same as for the open pattern algorithm
        else if (numNeighbors == 1)
        {
            if (neighbors[0].squaredDistance < killDistSquared)
                mask[i] = true;
        }

        // attractors with multiple neighbors
        else
        {
            // kill attractors that are connected to the two vertices of the same element
            if (mask[i] = [&]()
            {
                for (int k = 0; k < neighbors.size(); ++k)
                {
                    const auto& g = veinNeighborGraph[neighbors[k].index];
                    for (int j = k+1; j < neighbors.size(); ++j)
                        if (g.end() != std::find(g.begin(), g.end(), neighbors[j].index))
                            return true; // found "short circuit" -> kill attractor
                }
                return false;

            }(); mask[i]) continue; // if the attractor is killed we are done

            // find all vein points (mergeCandidates) that are within merge distance of the attractor
            const auto& tree = graph.boundingBoxTree();
            intersectionEntitiesSphere(attractorPoints[i], mergeDistSquared, tree, mergeCandidates);

            if (!mergeCandidates.empty())
            {
                mergeCandidates.erase(
                    std::remove_if(mergeCandidates.begin(), mergeCandidates.end(),
                        // remove if not a tip or not in the neighbor graph
                        [&](const auto& c){
                            return !isTip[c]
                                || std::find_if(neighbors.begin(), neighbors.end(),
                                    [&](const auto& n){ return n.index == c; }) == neighbors.end();
                        }),
                    mergeCandidates.end()
                );

                if (!mergeCandidates.empty())
                {
                    const auto numNeighborTipCandidates = std::count_if(
                        neighbors.begin(), neighbors.end(),
                        [&](const auto& n){
                            return isTip[n.index]
                                && std::find(mergeCandidates.begin(), mergeCandidates.end(), n.index) != mergeCandidates.end();
                        }
                    );

                    // we only merge if all attracted neighbors have reached the merge distance
                    if (numNeighborTipCandidates == numNeighbors && numNeighborTipCandidates == mergeCandidates.size())
                    {
                        for (const auto c : mergeCandidates)
                            mapVertexToMergerPoint[c] = newPoints.size();
                        newPoints.push_back(attractorPoints[i]);
                        mask[i] = true; // mark attractor point for removal
                    }

                    mergeCandidates.clear(); // clear buffer for next iteration
                }
            }
        }
    }

    // perform mergers
    if (!newPoints.empty())
    {
        graph.grid().preGrow();

        // insert new points into vein graph
        std::vector<unsigned int> newVertexIndex(newPoints.size());
        for (unsigned int i = 0; i < newPoints.size(); ++i)
            newVertexIndex[i] = graph.grid().insertVertex(std::move(newPoints[i]));

        // insert new segments, remove old segments
        for (const auto& element : elements(gridView))
        {
            const auto tipVertex = gridView.indexSet().subIndex(element, 1, dim);
            if (auto newPIdx = mapVertexToMergerPoint[tipVertex]; newPIdx >= 0)
            {
                const unsigned int baseVertex = gridView.indexSet().subIndex(element, 0, dim);
                graph.grid().insertElement(
                    Dune::GeometryTypes::line,
                    std::vector<unsigned int>({ baseVertex, newVertexIndex[newPIdx] })
                );

                // the new element replaces this element
                graph.grid().removeElement(element);
            }
        }

        graph.grid().grow();
        graph.grid().postGrow();

        std::cout << Fmt::format("Performed {} mergers\n", newVertexIndex.size());

        graph.update();
    }

    // remove handled points from the list of attractors
    attractor.prune(mask);
}

} // end namespace Dumux::LeafVeinGenerator

#endif
