// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geometry
 * \brief Find all entities intersection a given sphere
 */
#ifndef DUMUX_GEOMETRY_INTERSECTING_ENTITIES_SPHERE_HH
#define DUMUX_GEOMETRY_INTERSECTING_ENTITIES_SPHERE_HH

namespace Dumux::Detail {

/*!
 * \ingroup Geometry
 * \brief Compute the entities in an AABB tree intersecting a sphere
 * \note specialization for point geometries (point cloud AABB tree)
 */
template<class EntitySet, class ctype, int dimworld,
         typename std::enable_if_t<(EntitySet::Entity::Geometry::mydimension == 0), int> = 0>
void intersectionEntitiesSphere(const Dune::FieldVector<ctype, dimworld>& center,
                                const ctype radiusSquared,
                                const BoundingBoxTree<EntitySet>& tree,
                                std::size_t node,
                                std::vector<std::size_t>& result)
{
    // Get the bounding box for the current node
    const auto& bBox = tree.getBoundingBoxNode(node);

    // If the bounding box is a leaf check if we are inside the sphere
    if (tree.isLeaf(bBox, node))
    {
        const std::size_t entityIdx = bBox.child1;
        const auto& p = tree.entitySet().entity(entityIdx).geometry().corner(0);
        const auto squaredDistance = (p-center).two_norm2();

        if (squaredDistance <= radiusSquared)
            result.push_back(entityIdx);
    }

    // Check the children nodes recursively
    else
    {
        // If bounding box is outside radius, then don't search further
        const auto squaredDistance = squaredDistancePointBoundingBox(
            center, tree.getBoundingBoxCoordinates(node)
        );

        // do not continue if the AABB is further away than the radius
        if (squaredDistance > radiusSquared) return;

        intersectionEntitiesSphere(center, radiusSquared, tree, bBox.child0, result);
        intersectionEntitiesSphere(center, radiusSquared, tree, bBox.child1, result);
    }
}

} // end namespace Dumux::Detail

namespace Dumux {


/*!
 * \ingroup Geometry
 * \brief Compute the entities in an AABB tree intersecting a sphere
 * \note specialization for point geometries (point cloud AABB tree)
 * \note elements will be added to the back of the result vector in no particular order
 */
template<class EntitySet, class ctype, int dimworld>
void intersectionEntitiesSphere(const Dune::FieldVector<ctype, dimworld>& center,
                                const ctype radiusSquared,
                                const BoundingBoxTree<EntitySet>& tree,
                                std::vector<std::size_t>& result)
{
    Detail::intersectionEntitiesSphere(
        center, radiusSquared, tree, tree.numBoundingBoxes() - 1, result
    );
}

/*!
 * \ingroup Geometry
 * \brief Compute the entities in an AABB tree intersecting a sphere
 * \note specialization for point geometries (point cloud AABB tree)
 * \return vector of intersecting entity indices
 */
template<class EntitySet, class ctype, int dimworld>
std::vector<std::size_t> intersectionEntitiesSphere(const Dune::FieldVector<ctype, dimworld>& center,
                                                    const ctype radiusSquared,
                                                    const BoundingBoxTree<EntitySet>& tree)
{
    std::vector<std::size_t> result;
    Detail::intersectionEntitiesSphere(
        center, radiusSquared, tree, tree.numBoundingBoxes() - 1, result
    );
    return result;
}

} // end namespace Dumux

#endif
