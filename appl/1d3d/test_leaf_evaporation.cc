#include <config.h>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include "leafcouplingmanager.hh"
#include "properties.hh"
#include "h2oair_helperfunctions.hh"

#include <dumux/material/fluidstates/compositional.hh>

#include <iostream>
#include <ostream>
#include <iomanip>
#include <dumux/io/format.hh>
#include <dumux/io/container.hh>

#include <dumux/io/gnuplotinterface.hh> //For vs code: https://stackoverflow.com/questions/75921414/java-symbol-lookup-error-snap-core20-current-lib-x86-64-linux-gnu-libpthread

using namespace Dumux;

// Cell Wall
struct PARAMS_CW
{
    static constexpr double r = 10e-9; // pore radius in m
    static constexpr double L = 1e-6; // available retreat length in m
    static constexpr double T = 273.15 + 20; // temperature in the leaf air space
    static constexpr double p_tissue = 1e5; // pressure in the leaf air space
    static constexpr double p_xylem = -1.5e6; // pressure in the xylem
    static constexpr double Hrel = 0.5; // Relative humidity in the leaf air space
    static constexpr double p_xylem_low = -40e6;
    static constexpr double p_xylem_high = 10e5;
};

struct PARAMS_VINCENT
{
    static constexpr double r = 1.4e-9; // pore radius in m,
    static constexpr double L = 1e-2; // available retreat length in m
    static constexpr double T = 273.15 + 20; // temperature in the leaf air space
    static constexpr double p_tissue = 1e5; // pressure in the leaf air space
    static constexpr double p_xylem = -1.5e6; // pressure in the xylem
    static constexpr double Hrel = 0.5; // Relative humidity in the leaf air space
    static constexpr double p_xylem_low = -40e6;
    static constexpr double p_xylem_high = 1e5;
};

// Bundle Sheath cell size as retreat length but same pore size as cell wall
struct PARAMS_BS
{
    static constexpr double r = 10e-9; // pore radius in m, 10 nm
    static constexpr double L = 60e-6; // available retreat length in m, 60 µm
    static constexpr double T = 273.15 + 20; // temperature in the leaf air space
    static constexpr double p_tissue = 1e5; // pressure in the leaf air space
    static constexpr double p_xylem = -1.5e6; // pressure in the xylem
    static constexpr double Hrel = 0.5; // Relative humidity in the leaf air space

    static constexpr double p_xylem_low = -40e6;//before -40e6
    static constexpr double p_xylem_high = 1e5;//before 10e5
};

// Stomata from stable transpiration paper
struct PARAMS_ST
{
    static constexpr double r = 80e-9; // pore radius in m, 80 nm
    static constexpr double L = 7e-3; // available retreat length in m, 7 mm
    static constexpr double T = 273.15 + 20; // temperature in the leaf air space
    static constexpr double p_tissue = 1e5; // pressure in the leaf air space
    static constexpr double p_xylem = -1.5e6; // pressure in the xylem
    static constexpr double Hrel = 0.5; // Relative humidity in the leaf air space

    static constexpr double p_xylem_low = -40e6;
    static constexpr double p_xylem_high = 1e5;
};

// Alias PARAMS to the desired setup.
using PARAMS = PARAMS_BS;

template<class LeafInterfaceModelType, CapillaryModel CapillaryModelType = CapillaryModel::Standard>
void plot_varying_temperature(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Varying Temperature" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;

    GnuplotInterface<Scalar> gnuplot;
    std::vector<Scalar> T;
    std::vector<Scalar> flux;
    Scalar T_min = 273.15 + 10;
    Scalar T_max = 273.15 + 90;

    std::ofstream temperature_file("varying_temperature.csv");
    temperature_file << "temperature,retreat_length,flux,regime,X_h2o_tissue,Hrel" << std::endl;

    int n = 10;
    for (Scalar Hrel : {0.5, 0.95})
    {
        for (int i = 0; i < n; ++i)
        {
            Scalar T_i = T_min + i * (T_max - T_min) / (n - 1);
            Scalar px = -1.5e5;
            Scalar X_h2o_tissue_i = LeafInterfaceModelType::H2OAirHelper::massFraction(Hrel, PARAMS::p_tissue, T_i);
            auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModelType>(PARAMS::r, PARAMS::r, PARAMS::L, px, PARAMS::p_tissue, T_i, T_i, X_h2o_tissue_i);
            T.push_back(T_i-273.15);
            std::cout << "T = " << T.back() << " Regime = " << (int)cd.regime << " mass fraction = " << X_h2o_tissue_i << " retreat = " << cd.retreat << std::endl;
            // static const Scalar secondToMinute = 60.0;
            flux.push_back(cd.flux);//*secondToMinute);
            // std::cout << "H2O vapor pressure at T = " << T_i << " is " << LeafInterfaceModelType::H2O::vaporPressure(T_i) << std::endl;
            temperature_file << T_i << ", " << cd.retreat << ", " << cd.flux << ", " << (int)cd.regime << ", " << X_h2o_tissue_i << ", " << Hrel << std::endl;
        }
    }

    temperature_file.close();

    // Plot
    gnuplot.resetPlot();
    // gnuplot.setYRange(0.0, 1.5* *std::max_element(flux.begin(), flux.end()));
    gnuplot.setXlabel("Temperature T in °C");
    gnuplot.setYlabel("flux [kg/m^2/s]");
    gnuplot.addDataSetToPlot(T, flux, "Temperature_Flux.dat", "w l t 'flux from T = "+Fmt::format("{}", T_min)+" to "+Fmt::format("{}", T_max)+"'");
    gnuplot.plot("temperature_flux.png");
}

template<class LeafInterfaceModelType, CapillaryModel CapillaryModelType = CapillaryModel::Standard>
void plot_varying_humidity(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Varying humidity" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;

    GnuplotInterface<Scalar> gnuplot;
    std::vector<Scalar> Hrel;
    std::vector<Scalar> retreat_length;

    int n = 100;
    for (int i = 0; i < n; ++i)
    {
        Scalar Hrel_i = i * (1.0 - (-0.1)) / (n - 1);
        Scalar X_h2o_tissue_i = LeafInterfaceModelType::H2OAirHelper::massFraction(Hrel_i, PARAMS::p_tissue, PARAMS::T);
        auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModelType>(PARAMS::r, PARAMS::r, PARAMS::L, PARAMS::p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue_i);
        Hrel.push_back(Hrel_i);
        retreat_length.push_back(cd.retreat);
    }

    // Plot
    gnuplot.resetPlot();
    gnuplot.setXRange(-0.1, 1.1);
    // gnuplot.setYRange(0.0, L/10);
    gnuplot.setXlabel("Hrel");
    gnuplot.setYlabel("retreat length [m]");
    gnuplot.addDataSetToPlot(Hrel, retreat_length, "X_h2o_tissue.dat", "w l t 'retreat length at p_xylem = "+Fmt::format("{}", PARAMS::p_xylem)+"'");
    gnuplot.plot("X_h2o_tissue.png");
}

template<class LeafInterfaceModelType>
void plot_varying_xylem_pressure(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Varying xylem pressure" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;

    typedef std::vector<Scalar> Vector;
    typedef std::pair<Vector, Vector> ModelResult;
    typedef std::map<std::string, ModelResult> ModelResults;
    ModelResults results;

    GnuplotInterface<Scalar> gnuplot;
    Vector p_xylem;

    Scalar p_xylem_low = PARAMS::p_xylem_low;
    Scalar p_xylem_high = PARAMS::p_xylem_high;

    Scalar X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(PARAMS::Hrel, PARAMS::p_tissue, PARAMS::T);

    std::vector<std::string> regularized_models ={};
    for (int i = 2; i <= 5; ++i) {
        regularized_models.push_back("reg_"+std::to_string(i));
    }
    std::vector<std::string> models = {"fixed", "frustum_2"};
    models.insert(models.end(), regularized_models.begin(), regularized_models.end());

    int n = 1000;
    for (int i = 0; i < n; ++i)
    {
        Scalar p_xylem_i = p_xylem_low + i * (p_xylem_high - p_xylem_low) / (n - 1);
        p_xylem.push_back(p_xylem_i);

        auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Standard>(PARAMS::r, PARAMS::L, p_xylem_i, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);
        results["fixed"].first.push_back(cd.flux);
        results["fixed"].second.push_back(cd.retreat);
        cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Frustum>(PARAMS::r/2, PARAMS::r*2, PARAMS::L, p_xylem_i, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);
        results["frustum_2"].first.push_back(cd.flux);
        results["frustum_2"].second.push_back(cd.retreat);

        for (auto regularized_model : regularized_models) {
            // get the number of the regularized model
            int regularized_model_number = std::stoi(regularized_model.substr(regularized_model.size()-1));
            Scalar rmin = PARAMS::r / regularized_model_number;
            Scalar rmax = PARAMS::r * regularized_model_number;
            auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Regularized>(rmin, rmax, PARAMS::L, p_xylem_i, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);
            results[regularized_model].first.push_back(cd.flux);
            results[regularized_model].second.push_back(cd.retreat);
        }
    }

    gnuplot.resetPlot();
    gnuplot.setXRange(p_xylem_low, p_xylem_high);
    gnuplot.setXlabel("p_xylem [Pa]");
    // gnuplot.setYlabel("retreat length [m]");
    gnuplot.setYlabel("flux [kg/m^2/s]");
    for (auto model : models) {
        gnuplot.addDataSetToPlot(p_xylem, results[model].first, model+"_flux.dat", "w l t '"+model+"'");
    }
    gnuplot.plot("p_xylem.png");

    // create a csv file for the retreat length and a csv file for the flux. Put all the different models in the columns and the different p_xylem in the rows.
    // container needs to be an iterable with begin and end
    writeContainerToFile(p_xylem, "p_xylem.csv");
    for (auto model : models) {
        writeContainerToFile(results[model].second, model+"_retreat.csv");
        writeContainerToFile(results[model].first,  model+"_flux.csv");
    }
}

template<class LeafInterfaceModelType>
void plot_2d(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "plot 2D" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;

    std::ofstream retreat_file("2d.csv");
    retreat_file << "Hrel,p_xylem,retreat_length,flux,X_h2o_tissue,regime" << std::endl;
    
    int n_Hrel = 1000;
    int n_p_xylem = 1000;

    Scalar p_low = -20e5;
    Scalar p_high = -5e5;

    for (int i = 0; i < n_Hrel; ++i)
    {
        for (int j = 0; j < n_p_xylem; ++j)
        {
            Scalar Hrel_i = i * (1.01 - (-0.01)) / (n_Hrel - 1);
            Scalar X_h2o_tissue_i = LeafInterfaceModelType::H2OAirHelper::massFraction(Hrel_i, PARAMS::p_tissue, PARAMS::T);
            Scalar p_xylem = p_low + j * (p_high - p_low) / (n_p_xylem - 1);
            auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Standard>(PARAMS::r, PARAMS::L, p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue_i);
            retreat_file << Hrel_i << ", " << p_xylem << ", " << cd.retreat << ", " << cd.flux << ", " << X_h2o_tissue_i << ", " << (int)cd.regime << std::endl;
        }
    }
    retreat_file.close();
}

template<class LeafInterfaceModelType>
void plot_kelvin_humidity(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Creating Kelvin plot" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;
    using H2OAir = typename LeafInterfaceModelType::H2OAir;
    using FluidState = typename LeafInterfaceModelType::FluidState;
    using H2OAirHelper = typename LeafInterfaceModelType::H2OAirHelper;

    GnuplotInterface<Scalar> gnuplot;
    std::vector<Scalar> pc;
    std::vector<Scalar> relativeHumidity;
    std::vector<Scalar> evaporationRate;

    FluidState fs;
    int n_pc = 1000;
    const Scalar pc_min = 0.0;
    const Scalar pc_max = H2OAirHelper::capillaryPressure(PARAMS::r);
    for (int i = 0; i < n_pc; ++i)
    {
        Scalar pci = pc_min + i * (pc_max - pc_min) / (n_pc - 1);
        fs.setPressure(H2OAir::gasPhaseIdx, 1e5);
        fs.setPressure(H2OAir::liquidPhaseIdx, 1e5 - pci);
        fs.setTemperature(273.15 + 22);
        const Scalar xh2o_sat_kelvin = H2OAirHelper::saturationMoleFraction(fs);
        fs.setPressure(H2OAir::liquidPhaseIdx, 1e5);
        const Scalar xh2o_sat_pc0 = H2OAirHelper::saturationMoleFraction(fs);
        fs.setMoleFraction(H2OAir::gasPhaseIdx, H2OAir::H2OIdx, xh2o_sat_kelvin);
        fs.setMoleFraction(H2OAir::gasPhaseIdx, H2OAir::AirIdx, 1.0 - xh2o_sat_kelvin);
        const Scalar Hrel = xh2o_sat_kelvin/xh2o_sat_pc0;

        pc.push_back(pci);
        relativeHumidity.push_back(Hrel);

        // calculate a theoretical evaporation rate, if the humidity outside is 0.5
        Scalar X_h2o_ext = H2OAirHelper::massFraction(Hrel, PARAMS::p_tissue, PARAMS::T);
        Scalar D =LeafInterfaceModelType::diffusionCoefficient(0.0, fs, false);
        Scalar evap = D * (fs.massFraction(H2OAir::gasPhaseIdx, H2OAir::H2OIdx) - X_h2o_ext) / PARAMS::r;
        evaporationRate.push_back(evap);
        // write out
        std::ofstream kelvin_file("kelvin.csv");
        kelvin_file << "pc,relative_humidity,evaporation_rate" << std::endl;
        for (int i = 0; i < pc.size(); ++i)
        {
            kelvin_file << pc[i] << ", " << relativeHumidity[i] << ", " << evaporationRate[i] << std::endl;
        }
        kelvin_file.close();
    }
    // Plot
    gnuplot.resetPlot();
    gnuplot.setXRange(pc_min, pc_max);
    gnuplot.setXlabel("capillary pressure [Pa]");
    gnuplot.setYlabel("relative humidity");
    gnuplot.addDataSetToPlot(pc, relativeHumidity, "kelvin.dat", "w l t 'Kelvin'");
    gnuplot.plot("kelvin_humidity.png");

    gnuplot.resetPlot();
    gnuplot.setXRange(pc_min, pc_max);
    gnuplot.setXlabel("capillary pressure [Pa]");
    gnuplot.setYlabel("evaporation rate [kg/m^2/s]");
    gnuplot.addDataSetToPlot(pc, evaporationRate, "kelvin_evaporation.dat", "w l t 'Kelvin'");
    gnuplot.plot("kelvin_evaporation.png");
}

template<class LeafInterfaceModelType>
void test_frustum_coupling(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Frustum coupling" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;

    Scalar rmin = 0.5*PARAMS::r;
    Scalar rmax = 2.0*PARAMS::r;
    Scalar X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(PARAMS::Hrel, PARAMS::p_tissue, PARAMS::T);
    auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Frustum>(rmin, rmax, PARAMS::L, PARAMS::p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);
    std::cout << "Q = " << cd.flux << std::endl;
    std::cout << "retr = " << cd.retreat << std::endl;
    std::cout << "regime = " << (int)cd.regime << std::endl;
}

// TODO: custom pc_max currently not supported
// template<class LeafInterfaceModelType>
// void stable_transpiration_test(LeafInterfaceModelType leafInterfaceModel)
// {
//     std::cout << "Stable transpiration test" << std::endl;
//     using Scalar = typename LeafInterfaceModelType::Scalar;

//     Scalar pc_custom = 33.5e3; // Paper self stabilized transpiration observed a capillary pressure different from the one calculated from the pore radius

//     Scalar X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(PARAMS::Hrel, PARAMS::p_tissue, PARAMS::T);

//     typename LeafInterfaceModelType::CouplingData cd = leafInterfaceModel.calculateCoupling(PARAMS::r, PARAMS::r, PARAMS::L, PARAMS::p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue, pc_custom);
//     std::cout << "retr = " << cd.retreat << std::endl;
//     std::cout << "L = " << cd.L << std::endl;
//     std::cout << "relative retreat = " << cd.retreat/cd.L << std::endl;
//     std::cout << "Q = " << cd.flux << std::endl;

//     // Scale the flux from a single pore to an area:
//     Scalar porosity = 0.32;
//     Scalar tortuosity = 3.5;
//     Scalar diameter = 105e-3;
//     Scalar area = 0.25 * M_PI * diameter * diameter;
//     Scalar totalMassFlux = cd.flux * porosity / tortuosity * area;
//     Scalar M3ToMM3 = 1e9;
//     Scalar liquidVolumeFluxInMM3 = totalMassFlux / LeafInterfaceModelType::H2O::liquidDensity(PARAMS::T,PARAMS::p_tissue) * M3ToMM3;
//     Scalar pressureDrop = cd.p_xylem - cd.fs_interface.pressure(LeafInterfaceModelType::liquidPhaseIdx);
//     std::cout << "liquid total volume flux in mm3 = " << liquidVolumeFluxInMM3 << std::endl;
//     std::cout << "pressure drop = " << pressureDrop << std::endl;
// }

template<class LeafInterfaceModelType>
void test_edge_cases(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Edge cases" << std::endl;
    // Same setup
    using Scalar = typename LeafInterfaceModelType::Scalar;
    Scalar p_xylem = 1e5; // pressure in the xylem

    // Edge case Hrel = 1
    std::cout << "Edge case Hrel = 1" << std::endl;
    Scalar Hrel = 0.999999999;
    Scalar X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(Hrel, PARAMS::p_tissue, PARAMS::T);
    auto cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Standard>(PARAMS::r, PARAMS::L, p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);

    // Edge case Hrel = 0
    std::cout << "Edge case Hrel = 0" << std::endl;
    Hrel = 0.000000001;
    X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(Hrel, PARAMS::p_tissue, PARAMS::T);
    cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Standard>(PARAMS::r, PARAMS::L, p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);

    // Backwards flow
    p_xylem = -100e5;
    std::cout << "Backwards flow" << std::endl;
    Hrel = 0.5;
    X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(Hrel, PARAMS::p_tissue, PARAMS::T);
    cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Standard>(PARAMS::r, PARAMS::L, p_xylem, PARAMS::p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);
    std::cout << "Q = " << cd.flux << std::endl;
}

template<class LeafInterfaceModelType>
void test_pressure_bomb(LeafInterfaceModelType leafInterfaceModel)
{
    std::cout << "Pressure Bomb" << std::endl;
    using Scalar = typename LeafInterfaceModelType::Scalar;

    // Pressure bomb variable: The pressure of the air surrounding (and filling the leaf) is increased until water is forced out of the xylem
    const Scalar tolerance = 0.0001 * 1e5;
    bool converged = false;
    Scalar p_tissue_low = -20e5;
    Scalar p_tissue_high = 100e5;

    typename LeafInterfaceModelType::CouplingData cd;
    Scalar p_tissue;
    while(!converged){
        // simple bisection
        p_tissue = (p_tissue_low + p_tissue_high) / 2;
        Scalar X_h2o_tissue = LeafInterfaceModelType::H2OAirHelper::massFraction(PARAMS::Hrel, p_tissue, PARAMS::T);
        cd = leafInterfaceModel.template calculateCoupling<CapillaryModel::Standard>(PARAMS::r, PARAMS::L, PARAMS::p_xylem, p_tissue, PARAMS::T, PARAMS::T, X_h2o_tissue);
        if (cd.flux > 1e-14){
            p_tissue_low = p_tissue;
        } else {
            p_tissue_high = p_tissue;
        }
        converged = p_tissue_high - p_tissue_low < tolerance;
    }

    std::cout << "Q = " << cd.flux << std::endl;
    std::cout << "retr = " << cd.retreat << std::endl;
    // std::cout << "p_tissue = " << p_tissue << std::endl;
    // std::cout << "p_xylem = " << PARAMS::p_xylem << std::endl;
    // std::cout << "pc_observed = " << cd.fs_interface.pressure(LeafInterfaceModelType::gasPhaseIdx) - cd.fs_interface.pressure(LeafInterfaceModelType::liquidPhaseIdx) << std::endl;
    // std::cout << "pc_max_calculated = " << leafInterfaceModel.capillaryPressure(PARAMS::r) << std::endl;
}

template<class LeafInterfaceModelType>
void test_water_potential(LeafInterfaceModelType leafInterfaceModel)
{
    using Scalar = typename LeafInterfaceModelType::Scalar;
    using H2OAirHelper = typename LeafInterfaceModelType::H2OAirHelper;
    // Check normalization of water potential
    const Scalar pGas = 1e5;
    const Scalar pLiquid = -1e5;
    const Scalar T = 273.15 + 20;
    const Scalar liquidPotential = H2OAirHelper::liquidWaterPotential(pLiquid);
    const Scalar gasPotential = H2OAirHelper::kelvinPressure(T,1.0);
    std::cout << liquidPotential << " " << gasPotential << std::endl;

    // Compare with FluidState variant
    using H2OAir = typename LeafInterfaceModelType::H2OAir;
    using FluidState = typename LeafInterfaceModelType::FluidState;
    FluidState fs;
    fs.setPressure(H2OAir::gasPhaseIdx, pGas);
    fs.setPressure(H2OAir::liquidPhaseIdx, pLiquid);
    fs.setTemperature(T);
    const Scalar xh2o_sat = H2OAirHelper::saturationMoleFraction(fs);
    fs.setMoleFraction(H2OAir::gasPhaseIdx, H2OAir::H2OIdx, xh2o_sat);
    fs.setMoleFraction(H2OAir::gasPhaseIdx, H2OAir::AirIdx, 1.0-xh2o_sat);
    const Scalar liquidPotential_fs = H2OAirHelper::waterPotential(fs, H2OAir::liquidPhaseIdx);
    const Scalar gasPotential_fs = H2OAirHelper::waterPotential(fs, H2OAir::gasPhaseIdx);
    std::cout << liquidPotential_fs << " " << gasPotential_fs << std::endl;
    // assert(std::abs(liquidPotential - liquidPotential_fs) < 1e-5);
    // assert(std::abs(gasPotential - gasPotential_fs) < 1e-5);
}

int main(int argc, char** argv)
{
    using namespace Dumux;

    initialize(argc, argv);
    Parameters::init(argc, argv);

    using TissueFlowTypeTag = Properties::TTag::Tissue;
    using Scalar = GetPropType<TissueFlowTypeTag, Properties::Scalar>;
    using H2OAir = GetPropType<TissueFlowTypeTag, Properties::FluidSystem>::MultiPhaseFluidSystem;
    using FluidState = CompositionalFluidState<Scalar, H2OAir>; //Cant use TTag, because we only use 1p systems in each subproblem, but we need a 2p at the interface

    using LeafInterfaceModelType = LeafInterfaceModel<Scalar, H2OAir, FluidState>;
    LeafInterfaceModelType leafInterfaceModel;

    plot_varying_temperature(leafInterfaceModel);
    plot_varying_humidity(leafInterfaceModel);
    plot_varying_xylem_pressure(leafInterfaceModel);
    plot_2d(leafInterfaceModel);
    plot_kelvin_humidity(leafInterfaceModel);
    // stable_transpiration_test(leafInterfaceModel);
    test_edge_cases(leafInterfaceModel);
    test_pressure_bomb(leafInterfaceModel);
    test_frustum_coupling(leafInterfaceModel);
    test_water_potential(leafInterfaceModel);

    return 0;
};