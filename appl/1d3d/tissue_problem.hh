// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TISSUE_FLOW_PROBLEM_HH
#define DUMUX_TISSUE_FLOW_PROBLEM_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

template <class TypeTag>
class TissueProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<PrimaryVariables::size()>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;

    static constexpr int dimworld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using H2OAir = typename FluidSystem::MultiPhaseFluidSystem;
    using H2O = typename FluidSystem::MultiPhaseFluidSystem::H2O;
    using Air = typename FluidSystem::MultiPhaseFluidSystem::Air;
    using H2OAirHelper = H2OAirHelperFunctions<H2OAir>;

public:
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    enum
    {
        // indices of the primary variables
        H2OIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx),
        AirIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::AirIdx),

        // indices of the equations
        contiH2OEqIdx = Indices::conti0EqIdx + H2OIdx,
        contiAirEqIdx = Indices::conti0EqIdx + AirIdx,
    };

    static constexpr int dim = GridView::dimension;

    TissueProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                  std::shared_ptr<CouplingManager> couplingManager
                  )
    : ParentType(gridGeometry, "Tissue")
    , couplingManager_(couplingManager)
    {
        FluidSystem::init();

        name_ = getParam<std::string>("Problem.Name") + "_3d";
        height_ = gridGeometry->bBoxMax()[2]-gridGeometry->bBoxMin()[2];
        TAtmos_ = getParam<Scalar>("Environment.TAtmos");
        pAtmos_ = getParam<Scalar>("Environment.pAtmos");
        Hrel_ = getParam<Scalar>("Environment.Hrel");
        boundaryLayerThickness_ = getParam<Scalar>("Environment.BoundaryLayerThickness");

        gHeatEpidermis_ = getParam<Scalar>("Interfaces.gHeatEpidermis");

        leafArea_ = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elementVolume = element.geometry().volume();
            leafArea_ += elementVolume;
        }
        leafArea_ /= height_;

        // Calculate quantities derived from the input parameters
        h2oMoleFractionAtmos_ = H2OAirHelper::moleFraction(Hrel_, pAtmos_, TAtmos_);
        h2oMassFractionAtmos_ = H2OAirHelper::massFraction(Hrel_, pAtmos_, TAtmos_);
        airMassFractionAtmos_ = 1.0 - h2oMassFractionAtmos_;

        diffusiveEvaporationRate_.resize(this->gridGeometry().gridView().size(0), 0.0);
        advectiveEvaporationRate_.resize(this->gridGeometry().gridView().size(0), 0.0);

        // print all indices from the enum
        std::cout << "Indices: " << std::endl;
        std::cout << "pressureIdx: " << Indices::pressureIdx << std::endl;
        std::cout << "H2OIdx: " << H2OIdx << std::endl;
        std::cout << "AirIdx: " << AirIdx << std::endl;
        if constexpr (ModelTraits::enableEnergyBalance()) std::cout << "temperatureIdx: " << Indices::temperatureIdx << std::endl;
        std::cout << "conti0EqIdx: " << Indices::conti0EqIdx << std::endl;
        std::cout << "contiH2OEqIdx: " << contiH2OEqIdx << std::endl;
        std::cout << "contiAirEqIdx: " << contiAirEqIdx << std::endl;
        if constexpr (ModelTraits::enableEnergyBalance()) std::cout << "energyEqIdx: " << Indices::energyEqIdx << std::endl;
    }

    const std::string& name() const
    { return name_; }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        bcTypes.setAllNeumann();
        return bcTypes;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initialAtPos(globalPos); }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        auto values = PrimaryVariables(0.0);
        values[Indices::pressureIdx] = pAtmos_;
        static const Scalar initialHrel = getParam<Scalar>("Tissue.SpatialParams.InitialHrel", 0.95);
        values[H2OIdx] = H2OAirHelper::massFraction(initialHrel, pAtmos_, TAtmos_);
        if constexpr (ModelTraits::enableEnergyBalance()) values[Indices::temperatureIdx] = TAtmos_;
        return values;
    }

    template<class ElementVolumeVariables, class ElementFluxVariablesCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        auto values = NumEqVector(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto scvIdx = scvf.insideScvIdx();
        const auto& volVars = elemVolVars[scvIdx];

        const Scalar temperature = volVars.temperature();
        const Scalar pressure = volVars.pressure();

        // energy flux through epidermis
        if constexpr (ModelTraits::enableEnergyBalance()) values[Indices::energyEqIdx] = -gHeatEpidermis_ * (TAtmos_ - temperature) / boundaryLayerThickness_;

        // Stomata flux
        if (onBottomBoundary(globalPos))
        {
            const Scalar D = 2.42e-5; // m^2/s
            const Scalar stomataPorosity = 0.04; //4% of the leaf surface is stomata
            const Scalar rho = volVars.density();

            // TODO: The advective flux is not physical currently!
            const Scalar advectiveFlux = -volVars.permeability() * volVars.mobility() * (pAtmos_-pressure) / boundaryLayerThickness_ * rho * stomataPorosity;
            const Scalar diffusiveH2OFlux = -D * (h2oMassFractionAtmos_ - volVars.massFraction(0, H2OIdx)) / boundaryLayerThickness_ * rho * stomataPorosity;
            const Scalar diffusiveAirFlux = -D * (airMassFractionAtmos_ - volVars.massFraction(0, AirIdx)) / boundaryLayerThickness_ * rho * stomataPorosity;
            const Scalar h2oLoss = diffusiveH2OFlux + advectiveFlux * volVars.massFraction(0, H2OIdx);
            const Scalar airLoss = diffusiveAirFlux + advectiveFlux * volVars.massFraction(0, AirIdx);
            // multiply evaporation flux with the area of the face to get the evaporation rate
            diffusiveEvaporationRate_[this->gridGeometry().elementMapper().index(element)] = diffusiveH2OFlux * scvf.area();
            advectiveEvaporationRate_[this->gridGeometry().elementMapper().index(element)] = advectiveFlux * volVars.massFraction(0, H2OIdx) * scvf.area();

            // std::cout << "advective vs diffusive H2O flux: " << advectiveFlux*volVars.massFraction(0, H2OIdx) << " vs " << diffusiveH2OFlux << std::endl;

            values[contiH2OEqIdx] = h2oLoss;
            values[contiAirEqIdx] = airLoss;

            // resulting additional enthalpy flux
            if constexpr (ModelTraits::enableEnergyBalance())
            {
                values[Indices::energyEqIdx] += h2oLoss * H2O::gasEnthalpy(temperature, pressure);
                values[Indices::energyEqIdx] += airLoss * Air::gasEnthalpy(temperature, pressure);
            }
        }
        return values;
    }

    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        this->couplingManager().template veinTissueCoupling<CouplingManager::tissueIdx>(source);
    }

    // mass source term in kg/s/m^3 with evaporation rate in kg/s/m^2
    // energy source term in J/m^3/s = W/m^3
    template<class ElementVolumeVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        auto values = NumEqVector(0.0);
        if constexpr (ModelTraits::enableEnergyBalance())
        {
            static const bool enableRadiation = getParam<bool>("Problem.EnableRadiation", false);
            if (enableRadiation){
                const Scalar T = elemVolVars[scv].temperature();
                // Long wave radiation
                static const Scalar emissivity_ff = 1.24 * pow(pAtmos_ * h2oMoleFractionAtmos_ / TAtmos_, 1./7.); // Dissertation Heck, eq 3.46
                static constexpr Scalar emissivity_pm = 0.95; // This is a value which should be not tooo bad, Heck 3.48
                static constexpr Scalar sigma = 5.67e-8; // Stefan-Boltzmann constant, units: W/m^2/K^4
                values[Indices::energyEqIdx] += 2.0 * sigma * emissivity_pm * (emissivity_ff * pow(TAtmos_, 4) - pow(T, 4)) / height_;
                // Short wave radiation
                const Scalar qSolar = 1000.0; // simple solar irradiation on earth surface in W/m^2
                const Scalar theta = M_PI/180.0 * 30.0; // angle of the sun rays with the surface normal
                const Scalar absportionCoefficient = 0.5*0.2 + 0.5*0.8; // Simple absorption coefficient calculated from infrared and visible light absorption
                const Scalar solarHeatInflux = qSolar * cos(theta) * absportionCoefficient;
                // Insert the radiation source into the energy equation
                values[Indices::energyEqIdx] += solarHeatInflux/height_; // Very simple for now - TODO: use sitesofevaporation (or moflo as intermediate step)!
                // Heat transport by fluid flow is handled in the neumann boundary condition
            }
        }
        return values;
    }

    bool onTopBoundary(const GlobalPosition &globalPos) const
    { return globalPos[2] >= this->gridGeometry().bBoxMax()[2]*(1.-eps_); }
    bool onBottomBoundary(const GlobalPosition &globalPos) const
    { return globalPos[2] <= this->gridGeometry().bBoxMin()[2]*(1.-eps_); }

    const Scalar leafArea() const
    {
        return leafArea_;
    }

    const Scalar height() const
    {
        return height_;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

public:
    std::string name_;

    Scalar TAtmos_;
    Scalar pAtmos_;
    Scalar boundaryLayerThickness_;
    Scalar Hrel_;

    Scalar gHeatEpidermis_;

    Scalar height_;
    Scalar leafArea_;

    static constexpr Scalar eps_ = 1e-6;
    std::shared_ptr<CouplingManager> couplingManager_;

    // derived quantities
    Scalar h2oMoleFractionAtmos_;
    Scalar h2oMassFractionAtmos_;
    Scalar airMassFractionAtmos_;
    std::vector<Scalar> mutable diffusiveEvaporationRate_;
    std::vector<Scalar> mutable advectiveEvaporationRate_;
};

} //end namespace Dumux

#endif
