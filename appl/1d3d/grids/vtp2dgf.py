#!/usr/bin/env python

"""
Convert a vtp file to a dgf file
Special script for ratbrain vtp file
"""
from __future__ import absolute_import
import argparse
import xml.etree.ElementTree as ET
from operator import attrgetter, itemgetter
import math
import sys
import os

if __name__ == "__main__":
    # read the command line arguments
    parser = argparse.ArgumentParser(description='vtp to dgf converter')
    parser.add_argument('vtpfile', type=str, help='The vtp file to convert')
    args = vars(parser.parse_args())

    # construct element tree from vtk file
    root = ET.fromstring(open(args['vtpfile']).read())
    piece = root[0][0]

    # read the xml format data arrays into lists
    connectivity = piece.find("Lines/DataArray[@Name='connectivity']").text.split()
    # todo more general using offsets to also work for other element types
    # offsets = piece.find("Lines/DataArray[@Name='offsets']").text.split()
    verticesPerElement = 2
    elements = [connectivity[x:x+verticesPerElement] for x in range(0, len(connectivity), verticesPerElement)]
    numElements = len(elements)

    # the vertices
    numCoord = int(piece.find("Points/DataArray").attrib["NumberOfComponents"])
    coords = piece.find("Points/DataArray").text.split()
    # split into vertex position vectors
    vertices = [coords[x:x+numCoord] for x in range(0, len(coords), numCoord)]
    for i, v in enumerate(vertices):
        vertices[i] = [float(j)*1e-3 for j in v] # convert to meter
    numVertices = len(vertices)

    # get bounding box and output as info
    bBoxMin = [sys.float_info.max, sys.float_info.max, sys.float_info.max]
    bBoxMax = [-i for i in bBoxMin]
    for v in vertices:
        for i, c in enumerate(v):
            bBoxMin[i] = min(c, bBoxMin[i])
            bBoxMax[i] = max(c, bBoxMax[i])
    print("Grid has a bounding box of {}x{}".format(bBoxMin, bBoxMax))

    # element data
    radii = piece.find("CellData/DataArray[@Name='radius']").text.split()
    radii = [float(i)*1e-3 for i in radii] # convert to meter

    # write DGF file
    outputfile = open(os.path.splitext(args["vtpfile"])[0] + ".dgf", "w")
    outputfile.write("DGF\n")
    outputfile.write("Vertex\n")
    for i, vertex in enumerate(vertices):
        for coord in vertex:
            outputfile.write("{} ".format(coord))
        outputfile.write("\n")
    outputfile.write("#\n")
    outputfile.write("SIMPLEX\n")
    outputfile.write("parameters 1\n") # radius
    for i, element in enumerate(elements):
        outputfile.write("{} {} ".format(element[0], element[1]))
        outputfile.write("{}\n".format(radii[i]))
    outputfile.write("#\n")
    outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")
