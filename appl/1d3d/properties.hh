// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_1D3D_FLOW_PROPERTIES_HH
#define DUMUX_1D3D_FLOW_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/subgrid/subgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/reorderingdofmapper.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/1p/incompressiblelocalresidual.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/components/simpleh2o.hh>

// #include <dumux/multidomain/embedded/couplingmanager1d3d_average.hh>
#include <dumux/multidomain/embedded/couplingmanager1d3d_surface.hh>

#include <dumux/multidomain/traits.hh>
#include "leafcouplingmanager.hh"

#include "vein_problem.hh"
#include "tissue_problem.hh"

#include "vein_spatialparams.hh"
#include "tissue_spatialparams.hh"

namespace Dumux::Properties {

namespace TTag { struct CachingPolicy {}; }

template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::CachingPolicy> { static constexpr bool value = false; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::CachingPolicy> { static constexpr bool value = false; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::CachingPolicy> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::CachingPolicy> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentMolecularDiffusion<TypeTag, TTag::CachingPolicy> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentHeatConduction<TypeTag, TTag::CachingPolicy> { static constexpr bool value = true; };

} // end namespace Dumux

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// VeinFlow model
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

namespace TTag { struct VeinFlow { using InheritsFrom = std::tuple<CachingPolicy, OneP, CCTpfaModel>; }; }

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::VeinFlow>
{ using type = Dune::FoamGrid<1, 3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::VeinFlow>
{ using type = VeinFlowProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::VeinFlow>
{ using type = VeinFlowSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>, double>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::VeinFlow>
{ using type = FluidSystems::OnePLiquid<double, Components::Constant<1, double> >; };

// Set the local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::VeinFlow>
{ using type = OnePIncompressibleLocalResidual<TypeTag>; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::VeinFlow> { static constexpr bool value = false; };

// if we have pt scotch use the reordering dof mapper to optimally sort the dofs (cc)
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::VeinFlow>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using ElementMapper = ReorderingDofMapper<GridView>;
    using VertexMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>;
    using MapperTraits = DefaultMapperTraits<GridView, ElementMapper, VertexMapper>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaDefaultGridGeometryTraits<GridView, MapperTraits>>;
};

} // end namespace Dumux::Properties

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// Tissue flow model
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

namespace TTag { struct Tissue { using InheritsFrom = std::tuple<CachingPolicy, CCTpfaModel, OnePNC>; }; }

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Tissue>
{ using type = Dune::SubGrid<3, Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Tissue>
{ using type = TissueProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Tissue>
{ using type = TissueSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>, double>; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::Tissue> { static constexpr bool value = false; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Tissue>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2OType = Components::TabulatedComponent<Components::SimpleH2O<Scalar>>; //Using SimpleH2O because normal H2O gave negative densities for negative pressures
    using H2OAir = FluidSystems::H2OAir<Scalar,H2OType, Dumux::FluidSystems::H2OAirDefaultPolicy</*useFastButSimplified*/true>, true>; // We use fastButSimplified because else my func saturationmolefraction does not work
    static constexpr auto phaseIdx = H2OAir::gasPhaseIdx; // Simulate the gas phase
    using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>;
};

} // end namespace Dumux::Properties

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// Multidomain coupling
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

using CouplingFlow = LeafCouplingManager<
    MultiDomainTraits<Properties::TTag::Tissue, Properties::TTag::VeinFlow>,
    Embedded1d3dCouplingMode::Surface
>;

template<class TypeTag> struct CouplingManager<TypeTag, TTag::Tissue> { using type = CouplingFlow; };
template<class TypeTag> struct PointSource<TypeTag, TTag::Tissue> { using type = CouplingFlow::PointSourceTraits::template PointSource<0>; };
template<class TypeTag> struct PointSourceHelper<TypeTag, TTag::Tissue> { using type = CouplingFlow::PointSourceTraits::template PointSourceHelper<0>; };

template<class TypeTag> struct CouplingManager<TypeTag, TTag::VeinFlow> { using type = CouplingFlow; };
template<class TypeTag> struct PointSource<TypeTag, TTag::VeinFlow> { using type = CouplingFlow::PointSourceTraits::template PointSource<1>; };
template<class TypeTag> struct PointSourceHelper<TypeTag, TTag::VeinFlow> { using type = CouplingFlow::PointSourceTraits::template PointSourceHelper<1>; };

} // end namespace Dumux::Properties

#endif
