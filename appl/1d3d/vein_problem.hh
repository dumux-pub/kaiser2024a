// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_VEIN_FLOW_PROBLEM_HH
#define DUMUX_VEIN_FLOW_PROBLEM_HH

#include <dumux/common/exceptions.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/porousmediumflow/problem.hh>

#include <mutex>

namespace Dumux {

template<class TypeTag>
class VeinFlowProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<PrimaryVariables::size()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
public:
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    template<class GridData>
    VeinFlowProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<CouplingManager> couplingManager,
                    std::shared_ptr<GridData> gridData
                    )
    : ParentType(gridGeometry, "Vein")
    , couplingManager_(couplingManager)
    {
        petiolePressure_ = getParam<Scalar>("Vein.BoundaryConditions.PetiolePressure");
        petioleTemperature_ = getParam<Scalar>("Vein.BoundaryConditions.PetioleTemperature");

        name_ = getParam<std::string>("Problem.Name") + "_1d";

        // read spatial parameters
        this->spatialParams().readGridParams(*gridData);

        veinLength_ = 0.0;
        veinVolume_ = 0.0;
        veinSurface_ = 0.0;

        const auto & gv = this->gridGeometry().gridView();
        for (const auto& element : elements(gv))
        {
            const Scalar elementLength = element.geometry().volume();
            veinLength_ += elementLength;

            const auto eIdx = this->gridGeometry().elementMapper().index(element);
            const auto radius = this->spatialParams().circleEquivalentRadius(eIdx);
            veinVolume_ += elementLength * M_PI * radius * radius;
            veinSurface_ += elementLength * 2 * M_PI * radius;
        }
    }

    const std::string& name() const
    { return name_; }

    BoundaryTypes boundaryTypes(const Element &element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes bcTypes;
        const auto &globalPos = scvf.ipGlobal();
        const Scalar littleOverPetiole = this->gridGeometry().bBoxMin()[2] * (1. - eps_); // minus because bBoxMin is negative
        if (globalPos[1] < littleOverPetiole) {
          bcTypes.setAllDirichlet();
          std::call_once(onceFlag,
                         [this, element] { petioleElement_ = element; });
        } else
          bcTypes.setAllNeumann();
        return bcTypes;
    }

    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        return initialAtPos(scvf.ipGlobal());
    }

    // doesn't really matter since we solve a stationary problem
    // but it can be a good guess for the Newton
    // We use it as boundary condition also, so does matter!
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        auto values = PrimaryVariables(0.0);
        values[Indices::pressureIdx] = petiolePressure_;
        if constexpr (ModelTraits::enableEnergyBalance()) values[Indices::temperatureIdx] = petioleTemperature_;
        return values;
    }

    // Positive values mean that mass is created, negative ones mean that it vanishes.
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().lowDimPointSources(); }

    // Positive values mean that mass is created, negative ones mean that it vanishes.
    // for the mass balance that would be a mass rate in \f$ [ kg / s ] \f$,
    // i.e. not volume-specific like `source`
    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        this->couplingManager().template veinTissueCoupling<CouplingManager::veinIdx>(source);
    }

    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    const Element& petioleElement() const
    { return petioleElement_; }

    const Scalar petiolePressure() const
    { return petiolePressure_; }

    const Scalar petioleTemperature() const
    { return petioleTemperature_; }

    const Scalar veinLength() const
    { return veinLength_; }

    const Scalar veinVolume() const
    { return veinVolume_; }

    const Scalar veinSurface() const
    { return veinSurface_; }

private:
    std::string name_;

    Scalar petiolePressure_;
    Scalar petioleTemperature_;

    mutable Element petioleElement_;
    mutable std::once_flag onceFlag;

    Scalar veinLength_;
    Scalar veinVolume_;
    Scalar veinSurface_;

    static constexpr Scalar eps_ = 1e-6;
    std::shared_ptr<CouplingManager> couplingManager_;
};

} //end namespace Dumux

#endif
