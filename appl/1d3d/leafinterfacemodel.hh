#ifndef DUMUX_LEAFINTERFACEMODEL_HH
#define DUMUX_LEAFINTERFACEMODEL_HH

#include <cmath>
#include <dumux/material/constants.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/throat/transmissibility1p.hh>
#include <dumux/porenetwork/common/throatproperties.hh>
#include <dumux/io/format.hh>
#include <dumux/nonlinear/findscalarroot.hh>

#include "h2oair_helperfunctions.hh"

namespace Dumux {

/// @brief The different regimes that the meniscus between the xylem and the leaf tissue can be in
enum class Regime
{
    EndPoint = 1,
    Balanced = 2,
    CompleteRetreat = 3,
    Spill = 4,
    NotCalculated = 0
};

/// @brief Convert a regime to a string
std::string regimeToString(Regime regime)
{
    switch (regime)
    {
    case Regime::EndPoint:
        return "EndPoint";
    case Regime::Balanced:
        return "Balanced";
    case Regime::CompleteRetreat:
        return "CompleteRetreat";
    case Regime::Spill:
        return "Spill";
    case Regime::NotCalculated:
        return "NotCalculated";
    default:
        return "Unknown";
    }
}

// Could be switch to a struct tag to follow the open-closed principle
/// @brief The different models for the coupling between the xylem and the leaf tissue
enum class CapillaryModel
{
    Standard,
    Regularized,
    Frustum,
};

/// @brief Convert a string to a coupling model
CapillaryModel capillaryModelFromString(const std::string& capillaryModel)
{
    if (capillaryModel == "Standard")
        return CapillaryModel::Standard;
    if (capillaryModel == "Regularized")
        return CapillaryModel::Regularized;
    if (capillaryModel == "Frustum")
        return CapillaryModel::Frustum;
    DUNE_THROW(Dune::InvalidStateException, "Unknown coupling model: " + capillaryModel);
}

/// @brief Describes and calculates the coupling between the xylem and the leaf tissue
/// @tparam ScalarType The type of the scalar values, e.g. double
/// @tparam H2OAirType The type of the fluid system, e.g. SimpleH2OAir
/// @tparam FluidStateType The type of the fluid state, e.g. CompositionalFluidState. Must be compatible with H2OAirType
template <class ScalarType, class H2OAirType, class FluidStateType>
class LeafInterfaceModel {

public:
    using Scalar = ScalarType;
    using H2OAir = H2OAirType;
    using FluidState = FluidStateType;

    using H2O = typename H2OAir::H2O;
    using Air = typename H2OAir::Air;

    using H2OAirHelper = H2OAirHelperFunctions<H2OAir>;

    static constexpr int gasPhaseIdx = H2OAir::gasPhaseIdx;
    static constexpr int liquidPhaseIdx = H2OAir::liquidPhaseIdx;

    /// @brief Stores the results of the coupling calculation
    struct CouplingData
    {
        Scalar retreat;
        Scalar flux;
        Regime regime;
    };

    /**
     * @brief Calculate the mass flux of a gas in a pore section filled with vapor
     * 
     * @param r The pore radius
     * @param fs_tissue The fluid state at the end of vapor section
     * @param fs_interface The fluid state of at the beginning of the vapor section
     * @param gasDistance The length of the vapor section
     * @return Scalar The vapor mass flux in [kg/m2/s]
     */
    static Scalar vaporMassFlux(Scalar r, const FluidState& fs_tissue, const FluidState& fs_interface, Scalar gasDistance)
    {
        const Scalar D = diffusionCoefficient(r, fs_interface);
        const Scalar X_h2o_eq = fs_interface.massFraction(gasPhaseIdx, H2OAir::H2OIdx);
        const Scalar X_h2o_out = fs_tissue.massFraction(gasPhaseIdx, H2OAir::H2OIdx);
        const Scalar rho_g = H2OAir::density(fs_interface, gasPhaseIdx);
    
        const Scalar flux = -rho_g*D*(X_h2o_out - X_h2o_eq)/gasDistance;
        return flux;
    }

    /**
     * @brief Calculate the mass flux of a liquid in a pore section filled with liquid
     * 
     * @param r The pore radius
     * @param xylemPressure The pressure in the xylem
     * @param fs_interface The fluid state at the end of the liquid section
     * @param liquidDistance The length of the liquid section
     * @return Scalar The liquid mass flux in [kg/m2/s]
     */
    static Scalar liquidMassFlux(Scalar r, Scalar xylemPressure, const FluidState& fs_interface, Scalar liquidDistance)
    {
        const Scalar k = r*r/8./H2OAir::viscosity(fs_interface, liquidPhaseIdx);
        const Scalar pl = fs_interface.pressure(liquidPhaseIdx);
        const Scalar rho_l = H2O::liquidDensity(fs_interface.temperature(), pl);
        const Scalar flux = -rho_l * k * (pl - xylemPressure) / liquidDistance;
        return flux;
    }

        /**
     * @brief Calculate the diffusion coefficient for a fluid in a pore, using the Knudsen formula or the binary diffusion coefficient
     * 
     * @param r The pore radius
     * @param fs The fluid state
     * @param useKnudsen Whether to use the Knudsen formula or the binary diffusion coefficient
     * @return Scalar The diffusion coefficient
     */
    static Scalar diffusionCoefficient(Scalar r, const FluidState& fs, bool useKnudsen=true)
    {
        const Scalar M = H2O::molarMass();
        const Scalar R = Constants<Scalar>::R;
        if (useKnudsen)
            return (2*r/3) * std::sqrt(8*R*fs.temperature(gasPhaseIdx)/(M_PI*M)); //Where is the formula for D Knudsen from?
        else
            return H2OAir::binaryDiffusionCoefficient(fs, gasPhaseIdx, gasPhaseIdx, liquidPhaseIdx);
    }

    /**
     * @brief Calculate the retreat length of a meniscus in a pore
     * 
     * @param xylemPressure The pressure in the xylem
     * @param fs_interface The fluid state at the meniscus
     * @param fs_tissue The fluid state in the tissue
     * @return Scalar The retreat length
     */
    static Scalar retreatLength(Scalar r, Scalar L, Scalar xylemPressure, const FluidState& fs_interface, const FluidState& fs_tissue)
    {
        const Scalar D = diffusionCoefficient(r, fs_interface);
        // const auto throatShape = Dumux::PoreNetwork::Throat::Shape::circle;
        // Dumux::PoreNetwork::TransmissibilityBruus<Scalar>::singlePhaseTransmissibility(throatShape, r, availableRetreatLength-retractLength, M_PI*r*r);
        const Scalar k = r*r/8./H2OAir::viscosity(fs_interface, liquidPhaseIdx);
        
        const Scalar X_h2o_eq = fs_interface.massFraction(gasPhaseIdx, H2OAir::H2OIdx);
        const Scalar X_h2o_out = fs_tissue.massFraction(gasPhaseIdx, H2OAir::H2OIdx);

        const Scalar pl = fs_interface.pressure(liquidPhaseIdx);

        const Scalar rho_l = H2O::liquidDensity(fs_interface.temperature(), pl);
        const Scalar rho_g = H2OAir::density(fs_interface, gasPhaseIdx);

        const Scalar beta = D/k * rho_g/rho_l; // This is not really the beta from the paper, if comparable then like 1/beta
        const Scalar Y =  beta * (X_h2o_out - X_h2o_eq) / (pl - xylemPressure);
        Scalar retreat = Y*L / (1+Y);

        // The formula is only valid for 0 < retreat < availableRetreatLength
        // Provide the user with the calculated in the error message
        if(retreat > L){
            auto message = Fmt::format("Retreat length is larger than the available retreat length. The used formula is not valid for this case. "
                          "Retreat length: {} Available retreat length: {}", retreat, L);
            // DUNE_THROW(NumericalProblem, message);
            std::cout << message << std::endl;
            // retreat = std::clamp(retreat, 0.0001*L, 0.9999*L);
        }
        if(retreat < 0){
            auto message = Fmt::format("Retreat length is smaller than zero. The used formula is not valid for this case. "
                          "Retreat length: {} Available retreat length: {}", retreat, L);
            message += Fmt::format("\nX_h2o_eq: {} X_h2o_out: {} pl: {} xylemPressure: {}",
                          X_h2o_eq, X_h2o_out, pl, xylemPressure);
            // DUNE_THROW(NumericalProblem, message);
            std::cout << message << std::endl;
            // retreat = std::clamp(retreat, 0.0001*L, 0.9999*L);
        }
        return retreat;
    }

    /// @brief Create tissue fluid state
    static FluidState createTissueFluidState(Scalar p_tissue, Scalar T_tissue, Scalar X_h2o_tissue)
    {
        FluidState fs_tissue;
        fs_tissue.setPressure(gasPhaseIdx, p_tissue);
        fs_tissue.setTemperature(T_tissue);
        fs_tissue.setMassFraction(gasPhaseIdx, H2OAir::H2OIdx, X_h2o_tissue);
        fs_tissue.setMassFraction(gasPhaseIdx, H2OAir::AirIdx, 1.0-X_h2o_tissue);
        return fs_tissue;
    }

    /// @brief Update interface fluid state
    static void updateInterfaceFluidState(FluidState& fs_interface, Scalar pc)
    {
        fs_interface.setPressure(liquidPhaseIdx, fs_interface.pressure(gasPhaseIdx) - pc);
        Scalar x_h2o_eq = H2OAirHelper::saturationMoleFraction(fs_interface);
        fs_interface.setMoleFraction(gasPhaseIdx, H2OAir::H2OIdx, x_h2o_eq);
        fs_interface.setMoleFraction(gasPhaseIdx, H2OAir::AirIdx, 1.0-x_h2o_eq);
    }

    /// @brief Calculate the meniscus / flow regime
    /// @param p_xylem The pressure in the xylem
    /// @param fs_interface The fluid state at the meniscus
    /// @param fs_tissue The fluid state in the tissue
    /// @param pc_max The maximal possible capillary pressure
    static Regime calculateRegime(Scalar p_xylem, const FluidState& fs_interface, const FluidState& fs_tissue, Scalar pc_max)
    {
        if ( p_xylem < fs_tissue.pressure(gasPhaseIdx) - pc_max ) // The negative pressure in the xylem is stronger than the capillary pressure
            return Regime::CompleteRetreat; // Complete retreat of the meniscus into the xylem
        else if (p_xylem > fs_tissue.pressure(gasPhaseIdx)) // The (positive) pressure in the xylem is stronger than the pressure in the tissue
            return Regime::Spill; // Water spills into the tissue
        else if ( fs_tissue.massFraction(gasPhaseIdx, H2OAir::H2OIdx) < fs_interface.massFraction(gasPhaseIdx, H2OAir::H2OIdx) ) // fs_interface must be updated with the maximal possible capillary pressure at the capillary endpoint, this can be different from pc_max!
            return Regime::Balanced; // Capillary pressure limited regime: partial retreat to a static equilibrium meniscus location
        else 
            return Regime::EndPoint; // Evaporation limited regime: meniscus in static equilibrium at capillary endpoint
    }

    // /**
    //  * @brief Calculate the coupling between the xylem and the leaf tissue
    //  * 
    //  * @param r The pore radius
    //  * @param p_xylem The pressure in the xylem
    //  * @param p_tissue The pressure in the tissue
    //  * @param T_xylem The temperature in the xylem
    //  * @param T_tissue The temperature in the tissue
    //  * @param X_h2o_tissue The mass fraction of water in the tissue
    //  * @return CouplingData Struct containing the flux and the retreat length
    //  */
    template<CapillaryModel CapillaryModelType, typename std::enable_if_t<CapillaryModelType == CapillaryModel::Standard, int> = 0>
    static CouplingData calculateCoupling(
        Scalar r, Scalar L,
        Scalar p_xylem, Scalar p_tissue,
        Scalar T_xylem, Scalar T_tissue,
        Scalar X_h2o_tissue)
    {
        return calculateCoupling<CapillaryModelType>(r, r, L, p_xylem, p_tissue, T_xylem, T_tissue, X_h2o_tissue);
    }

    /**
     * @brief Calculate the coupling between the xylem and the leaf tissue for a single pore
     * 
     * @param p_xylem The pressure in the xylem
     * @param p_tissue The pressure in the tissue
     * @param T_xylem The temperature in the xylem
     * @param T_tissue The temperature in the tissue
     * @param X_h2o_tissue The mass fraction of water in the tissue
     * @return CouplingData Struct containing the flux and the retreat length
     */
    //specialization for the standard model
    template<CapillaryModel CapillaryModelType, typename std::enable_if_t<CapillaryModelType == CapillaryModel::Standard, int> = 0>
    static CouplingData calculateCoupling(
        Scalar r_min, Scalar r_max, Scalar L,
        Scalar p_xylem, Scalar p_tissue,
        Scalar T_xylem, Scalar T_tissue,
        Scalar X_h2o_tissue)
    {
        if (r_min != r_max)
            DUNE_THROW(Dune::InvalidStateException, "The standard model is only valid for a single pore size, but r_min and r_max are different.");
        // Calculate the maximum capillary pressure
        const Scalar r = r_min;
        const Scalar pc_max = H2OAirHelper::capillaryPressure(r);

        FluidState fs_tissue = createTissueFluidState(p_tissue, T_tissue, X_h2o_tissue);

        // Calculate first part of the interface fluid state
        FluidState fs_interface;
        fs_interface.setPressure(gasPhaseIdx, p_tissue); // We assume zero pressure gradient in the air space in the pore
        fs_interface.setTemperature(T_tissue); // We assume zero temperature gradient in the air space in the pore

        // Check which regime we are in
        updateInterfaceFluidState(fs_interface, pc_max); // Just temporary!

        Scalar retreat, flux;
        Regime regime = calculateRegime(p_xylem, fs_interface, fs_tissue, pc_max);
        switch (regime)
        {
        case Regime::CompleteRetreat: {
            retreat = L;
            flux =  -std::numeric_limits<Scalar>::epsilon();//liquidMassFlux(r, p_xylem, fs_interface, L);
            break;
        }
        case Regime::Spill: {
            retreat = 0;
            const Scalar overPressureAreaMultiplicator = (p_xylem - p_tissue)*1e-7;
            flux = liquidMassFlux(r, p_xylem, fs_interface, L);
            flux *= (1. + overPressureAreaMultiplicator);
            break;
        }
        case Regime::Balanced: {
            retreat = retreatLength(r, L, p_xylem, fs_interface, fs_tissue);
            flux = liquidMassFlux(r, p_xylem, fs_interface, L-retreat);
            break;
        }
        case Regime::EndPoint: {
            fs_interface.setMassFraction(gasPhaseIdx, H2OAir::H2OIdx, X_h2o_tissue);
            fs_interface.setMassFraction(gasPhaseIdx, H2OAir::AirIdx, 1.0-X_h2o_tissue);
            // calc pc from tissue=meniscus mole fraction
            const Scalar T = fs_interface.temperature(gasPhaseIdx);
            const Scalar K = H2OAir::molarMass(H2OAir::H2OIdx) / H2OAir::density(fs_interface, H2OAir::H2OIdx) / Dumux::Constants<Scalar>::R / T;
            const Scalar pc = -std::log(fs_interface.partialPressure(gasPhaseIdx, H2OAir::H2OIdx) / H2O::vaporPressure(T)) / K;
            fs_interface.setPressure(liquidPhaseIdx, p_tissue - pc); // Correct the previously assigned pressure
            retreat = 0;
            flux = liquidMassFlux(r, p_xylem, fs_interface, L);
            break;
        }
        default:
            DUNE_THROW(Dune::InvalidStateException, "Unknown regime: " + regimeToString(regime));
        }
        return CouplingData{retreat, flux, regime};
    }

    /// @brief Calculate the coupling between the xylem and the leaf tissue using the regularized model
    template<CapillaryModel CapillaryModelType, typename std::enable_if_t<CapillaryModelType == CapillaryModel::Regularized, int> = 0>
    static CouplingData calculateCoupling(
        Scalar r_min, Scalar r_max, Scalar L,
        Scalar p_xylem, Scalar p_tissue,
        Scalar T_xylem, Scalar T_tissue,
        Scalar X_h2o_tissue)
    {
        // define local pore size distribution function
        const auto rho_p = [r_min, r_max](Scalar r) {
            return 1.0 / (r_max - r_min) / M_PI / r / r; // Assume a uniform pore size distribution between r_min and r_max
        };
        // define local pore area
        const auto A_p = [r_min, r_max](Scalar r) {
            return M_PI * r * r;
        };

        // numeric integration of the coupling over the pore size distribution
        const int n_integration = 10;
        const Scalar dr = (r_max - r_min) / n_integration;
        Scalar Q_regularized = 0;
        Scalar retreat_regularized = 0;
        for (int i = 0; i < n_integration; i++)
        {
            // TODO: This is not really nice that we have a really string side effect here, influecing the couplingCalculation here
            Scalar r = r_min + i * dr;
            CouplingData cd = calculateCoupling<CapillaryModel::Standard>(r, L, p_xylem, p_tissue, T_xylem, T_tissue, X_h2o_tissue);
            Q_regularized += cd.flux * rho_p(r) * A_p(r) * dr;
            retreat_regularized += cd.retreat * rho_p(r) * A_p(r) * dr;
        }

        return CouplingData{retreat_regularized, Q_regularized, Regime::NotCalculated};
    }

    /// @brief Calculate the flux and retreat length for a single pore which has the shape of a frustum with radii r_min and r_max and length L
    template<CapillaryModel CapillaryModelType, typename std::enable_if_t<CapillaryModelType == CapillaryModel::Frustum, int> = 0>
    static CouplingData calculateCoupling(
        Scalar r_min, Scalar r_max, Scalar L,
        Scalar p_xylem, Scalar p_tissue,
        Scalar T_xylem, Scalar T_tissue,
        Scalar X_h2o_tissue)
    {
        // Calculate the maximum capillary pressure
        const Scalar pc_max_at_r_min = H2OAirHelper::capillaryPressure(r_min);
        const Scalar pc_max_at_r_max = H2OAirHelper::capillaryPressure(r_max);

        FluidState fs_tissue = createTissueFluidState(p_tissue, T_tissue, X_h2o_tissue);;

        // Calculate first part of the interface fluid state
        FluidState fs_interface;
        fs_interface.setPressure(gasPhaseIdx, p_tissue); // We assume zero pressure gradient in the air space in the pore
        fs_interface.setTemperature(T_tissue); // We assume zero temperature gradient in the air space in the pore

        // Check which regime we are in
        updateInterfaceFluidState(fs_interface, pc_max_at_r_max); // Just temporary!

        Scalar retreat, flux;
        Regime regime = calculateRegime(p_xylem, fs_interface, fs_tissue, pc_max_at_r_min);
        switch (regime)
        {
        case Regime::CompleteRetreat: {
            retreat = L;
            flux =  -std::numeric_limits<Scalar>::epsilon();//liquidMassFlux(r, p_xylem, fs_interface, L);
            break;
        }
        case Regime::Spill: {
            retreat = 0;
            const Scalar overPressureAreaMultiplicator = (p_xylem - p_tissue)*1e-7;
            flux = liquidMassFlux(r_max, p_xylem, fs_interface, L);
            flux *= (1. + overPressureAreaMultiplicator);
            break;
        }
        case Regime::Balanced: {
            const auto radius = [L, r_min, r_max](const double retreat_local){ return r_max + (r_min - r_max) * retreat_local/L; };
            // auto const* const_my_class = this;
            const auto flux_residual = [radius, L, &fs_interface, fs_tissue, p_tissue, p_xylem](const double retreat_local){//,const_my_class
                const Scalar radius_local = radius(retreat_local);
                const Scalar pc_local = H2OAirHelper::capillaryPressure(radius_local);
                updateInterfaceFluidState(fs_interface, pc_local);
                const Scalar Ql = liquidMassFlux(radius_local, p_xylem, fs_interface, L-retreat_local);
                const Scalar Qg = vaporMassFlux(radius_local, fs_tissue, fs_interface, retreat_local);
                return Ql - Qg;
            };
            const Scalar absTol = 1e-13; // TODO: What is acceptable?
            const Scalar retreat_min = 1e-12;
            const Scalar retreat_max = L-1e-12;
            retreat = findScalarRootBrent(retreat_min, retreat_max, flux_residual, absTol);

            const Scalar radius_retr = radius(retreat);
            flux = liquidMassFlux(radius_retr, p_xylem, fs_interface, L-retreat);
            break;
        }
        case Regime::EndPoint: {
            fs_interface.setMassFraction(gasPhaseIdx, H2OAir::H2OIdx, X_h2o_tissue);
            fs_interface.setMassFraction(gasPhaseIdx, H2OAir::AirIdx, 1.0-X_h2o_tissue);
            // calc pc from tissue=meniscus mole fraction
            const Scalar T = fs_interface.temperature(gasPhaseIdx);
            const Scalar K = H2OAir::molarMass(H2OAir::H2OIdx) / H2OAir::density(fs_interface, H2OAir::H2OIdx) / Dumux::Constants<Scalar>::R / T;
            const Scalar pc = -std::log(fs_interface.partialPressure(gasPhaseIdx, H2OAir::H2OIdx) / H2O::vaporPressure(T)) / K;
            fs_interface.setPressure(liquidPhaseIdx, p_tissue - pc); // Correct the previously assigned pressure
            retreat = 0;
            flux = liquidMassFlux(r_max, p_xylem, fs_interface, L);
            break;
        }
        default:
            DUNE_THROW(Dune::InvalidStateException, "Unknown regime: " + regimeToString(regime));
        }
        return CouplingData{retreat, flux, regime};
    }
};

} // namespace Dumux

#endif // DUMUX_LEAFINTERFACEMODEL_HH