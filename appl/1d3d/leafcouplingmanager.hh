#ifndef DUMUX_MULTIDOMAIN_EMBEDDED_COUPLINGMANAGER_1D3D_LEAF_HH
#define DUMUX_MULTIDOMAIN_EMBEDDED_COUPLINGMANAGER_1D3D_LEAF_HH

#include <random>
#include <vector>
#include <array>
#include <memory>
#include <type_traits>
#include <unordered_map>

#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/common/std/type_traits.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/math.hh>
#include <dumux/common/indextraits.hh>
#include <dumux/geometry/distance.hh>

// #include <dumux/multidomain/embedded/couplingmanager1d3d_average.hh>
#include <dumux/multidomain/embedded/couplingmanager1d3d_surface.hh>

#include <dumux/common/parameters.hh>
#include <config.h>

#include <dumux/material/fluidstates/compositional.hh>
#include "leafinterfacemodel.hh"

#include <dumux/common/numeqvector.hh>

namespace Dumux {

template<class MDTraits, class CouplingReconstruction>
class LeafCouplingManager
: public Embedded1d3dCouplingManager<MDTraits, CouplingReconstruction>
{
public:
    using ThisType = LeafCouplingManager<MDTraits, CouplingReconstruction>;
    using ParentType = Embedded1d3dCouplingManager<MDTraits, CouplingReconstruction>;

    static constexpr int tissueIdx = typename MDTraits::template SubDomain<0>::Index();
    static constexpr int veinIdx = typename MDTraits::template SubDomain<1>::Index();

    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;
    template<std::size_t id> using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;
    template<std::size_t id> using PointSource = GetPropType<SubDomainTypeTag<id>, Properties::PointSource>;
    template<std::size_t id> using NumEqVector = Dumux::NumEqVector<GetPropType<SubDomainTypeTag<id>, Properties::PrimaryVariables>>;
    template<std::size_t id> using ModelTraits = GetPropType<SubDomainTypeTag<id>, Properties::ModelTraits>;

    using Scalar = typename MDTraits::Scalar;
    using FluidSystem = GetPropType<SubDomainTypeTag<tissueIdx>, Properties::FluidSystem>;
    using H2OAir = typename GetPropType<SubDomainTypeTag<tissueIdx>, Properties::FluidSystem>::MultiPhaseFluidSystem;
    using FluidState = CompositionalFluidState<Scalar, H2OAir>;
    using H2O = typename H2OAir::H2O;
    using Air = typename H2OAir::Air;

public:
    using ParentType::ParentType; //construnctor

    template <std::size_t id>
    void veinTissueCoupling(PointSource<id> &source) const
    {
        auto values = NumEqVector<id>(0.0);

        Scalar temperature3D;
        Scalar temperature1D;
        Scalar pressure3D;
        Scalar pressure1D;
        Scalar h2oMassFraction3D;

        if constexpr (ModelTraits<tissueIdx>::enableEnergyBalance())
            temperature3D = this->bulkPriVars(source.id())[Problem<tissueIdx>::Indices::temperatureIdx];
        else
            temperature3D = getParam<Scalar>("Tissue.SpatialParams.Temperature");
        
        if constexpr (ModelTraits<veinIdx>::enableEnergyBalance())
            temperature1D = this->lowDimPriVars(source.id())[Problem<veinIdx>::Indices::temperatureIdx];
        else
            temperature1D = getParam<Scalar>("Vein.SpatialParams.Temperature");


        if constexpr (id == veinIdx){
            pressure3D = this->bulkPriVars(source.id())[Problem<tissueIdx>::Indices::pressureIdx];
            pressure1D = this->lowDimPriVars(source.id())[Problem<veinIdx>::Indices::pressureIdx];
            h2oMassFraction3D = this->bulkPriVars(source.id())[Problem<tissueIdx>::H2OIdx];
        }

        else if constexpr (id == tissueIdx) {
            pressure3D = this->bulkPriVars(source.id())[Problem<tissueIdx>::Indices::pressureIdx];
            pressure1D = this->lowDimPriVars(source.id())[Problem<veinIdx>::Indices::pressureIdx];
            h2oMassFraction3D = this->bulkPriVars(source.id())[Problem<tissueIdx>::H2OIdx];
        }
        else
            DUNE_THROW(Dune::NotImplemented, "Allowed ids are veinIdx and tissueIdx");

        // TODO: These are actually class members, so I should move them to the class
        static const Scalar rPore = getParam<Scalar>("Interfaces.BundleSheathPoreRadius");
        static const Scalar bundleSheathThickness = getParam<Scalar>("Interfaces.BundleSheathThickness");
        static const Scalar L = bundleSheathThickness;
        static const std::string capillaryModelString = getParam<std::string>("Interfaces.CapillaryModel", "Standard");
        static const CapillaryModel capillaryModel = capillaryModelFromString(capillaryModelString);
        typename LeafInterfaceModelType::CouplingData cd;

        static const Scalar rMin = rPore * getParam<Scalar>("Interfaces.FactorRMin", 0.5);
        static const Scalar rMax = rPore * getParam<Scalar>("Interfaces.FactorRMax", 2.0);

        if (capillaryModel == CapillaryModel::Standard){
            cd = leafInterfaceModel_.template calculateCoupling<CapillaryModel::Standard>(rPore, L, pressure1D, pressure3D, temperature1D, temperature3D, h2oMassFraction3D);
        } else if (capillaryModel == CapillaryModel::Regularized){
            cd = leafInterfaceModel_.template calculateCoupling<CapillaryModel::Regularized>(rMin, rMax, L, pressure1D, pressure3D, temperature1D, temperature3D, h2oMassFraction3D);
        } else if (capillaryModel == CapillaryModel::Frustum){
            cd = leafInterfaceModel_.template calculateCoupling<CapillaryModel::Frustum>(rMin, rMax, L, pressure1D, pressure3D, temperature1D, temperature3D, h2oMassFraction3D);
        }
        else {
            DUNE_THROW(Dune::NotImplemented, "Supported coupling types in coupling manager are Standard, Regularized and Frustum");
        }

        // get the segment radius
        const Scalar radius = this->radius(source.id());
        const Scalar perimeter = 2*M_PI*(radius+bundleSheathThickness);

        // Scale to the full bundle sheath area by multiplying with the porosity, dividing by the tortuosity and multiplying with the perimeter
        // the length is included by the quadrature later
        const Scalar tortuosity = 3.5; // As liquid and gas pathways are influenced both in the same way by the factors of tortuosity and porosity, we can multiply the flux at the end of the calculation with the porosity/tortuosity ratio
        static const Scalar porosity = getParam<Scalar>("Interfaces.BundleSheathPorosity");
        const Scalar flux = cd.flux*perimeter * porosity / tortuosity; // flux from vein to tissue in kg/m^2/s, gets integrated to kg/m/s
        static const Scalar conductivityVeinTissue_ = getParam<Scalar>("Interfaces.ConductionCoefficientVeinTissue");
        const Scalar temperatureExchange = -perimeter*conductivityVeinTissue_ * (temperature3D - temperature1D) / bundleSheathThickness;

        if constexpr (id == veinIdx){
            values[Problem<veinIdx>::Indices::conti0EqIdx] = -flux;
            if constexpr (ModelTraits<veinIdx>::enableEnergyBalance()) values[Problem<veinIdx>::Indices::energyEqIdx] = -temperatureExchange - flux * H2O::gasEnthalpy(temperature3D, pressure3D);
        }
        else if constexpr (id == tissueIdx) {
            values[Problem<tissueIdx>::contiH2OEqIdx] = flux;
            values[Problem<tissueIdx>::contiAirEqIdx] = 0.0;
            if constexpr (ModelTraits<tissueIdx>::enableEnergyBalance()) values[Problem<tissueIdx>::Indices::energyEqIdx] = temperatureExchange + flux * H2O::gasEnthalpy(temperature3D, pressure3D);
        }
        else
            DUNE_THROW(Dune::NotImplemented, "Allowed ids are veinIdx and tissueIdx");

        values *= source.quadratureWeight()*source.integrationElement(); // Integration from [...]/m to [...]
        source = values;
    }

private:

    using CouplingFluidState = CompositionalFluidState<Scalar, H2OAir>; //Cant use TTag, because we only use 1p systems in each subproblem, but we need a 2p at the interface
    using LeafInterfaceModelType = LeafInterfaceModel<Scalar, H2OAir, CouplingFluidState>;
    LeafInterfaceModelType leafInterfaceModel_;
}; // end class LeafCouplingManager

//! we support multithreaded assembly
template<class MDTraits, class CouplingReconstruction>
struct CouplingManagerSupportsMultithreadedAssembly<LeafCouplingManager<MDTraits, CouplingReconstruction>> : public std::true_type {};


} // end namespace Dumux

#endif //DUMUX_MULTIDOMAIN_EMBEDDED_COUPLINGMANAGER_1D3D_LEAF_HH