// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Multi-domain vein-leaf model
 */
#include <config.h>

#include <dune/common/timer.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/geometry/distance.hh>
#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/container.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/grid/gridmanager_foam.hh>

#include <dumux/adaptive/markelements.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include "properties.hh"
#include "../veins/leafshape.hh"

namespace Dumux {

// helper function to make sure both grid have roughly the same element sizes (h_max)
template<class Grid1D>
void refine1DGrid(Grid1D& grid1D)
{
    const auto& gridView = grid1D.leafGridView();
    const auto lowerLeft = getParam<Dune::FieldVector<double, 3>>("3D.Grid.LowerLeft");
    const auto upperRight = getParam<Dune::FieldVector<double, 3>>("3D.Grid.UpperRight");
    const auto length = upperRight - lowerLeft;
    const auto cells = getParam<Dune::FieldVector<double, 3>>("3D.Grid.Cells");
    const auto factor = getParam<double>("1D.Grid.MaxElementLengthFactor", 1.0);
    const auto maxElementLength = std::max({length[0]/cells[0], length[1]/cells[1], length[2]/cells[2]})*factor;
    std::cout << Fmt::format(
        "Refining grid with {} elements until max length: {:.2e}\n",
        gridView.size(0), maxElementLength
    );

    bool markedElements = true;
    while (markedElements)
    {
        markedElements = markElements(
            grid1D,
            [&](const auto& element){
                return element.geometry().volume() > maxElementLength ? 1 : 0;
            },
            false
        );

        if (markedElements)
        {
            grid1D.preAdapt();
            grid1D.adapt();
            grid1D.postAdapt();
        }
    }

    auto maxLength = 0.0;
    auto minLength = 1.0;
    for (const auto& element : elements(gridView))
    {
        const auto length = element.geometry().volume();
        maxLength = std::max(maxLength, length);
        minLength = std::min(minLength, length);
    }

    std::cout << Fmt::format("After refinement: {} elements\n", gridView.size(0))
              << Fmt::format("  -- max length: {:.2e}\n", maxLength)
              << Fmt::format("  -- min length: {:.2e}\n", minLength)
              << Fmt::format("  -- max level: {}\n", grid1D.maxLevel());
}

} // end namespace Dumux

int main(int argc, char** argv)
{
    using namespace Dumux;

    Dumux::initialize(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

     // Define the sub problem type tags
    using TissueFlowTypeTag = Properties::TTag::Tissue;
    using VeinFlowTypeTag = Properties::TTag::VeinFlow;

    // start the timer
    Dune::Timer timer;

    // leaf shape for subgrid
    const double finalLeafHeight = getParam<double>("LeafShape.ApexHeight");
    const double initialLeafScale = getParam<double>("LeafGrowth.InitialLeafHeight")/finalLeafHeight;
    LeafVeinGenerator::LeafShape<3> leafShape;
    leafShape.setScale(initialLeafScale);

    // cut out elements within the stair-case region
    auto elementSelector = [&](const auto& element)
    {
        const auto globalPos = element.geometry().center();
        const auto x = globalPos[0];
        const auto y = globalPos[1];

        const auto apexHeight = 1e-3*leafShape.bBoxMax()[1];
        const auto petioleHeight = 1e-3*leafShape.bBoxMin()[1];
        const auto width = 1e-3*leafShape.width(y*1e3);

        if (y < apexHeight && y > petioleHeight && x < width && x > -width)
            return true;
        return false;
    };

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    GridManager<GetPropType<TissueFlowTypeTag, Properties::Grid>> bulkGridManager;
    bulkGridManager.init(elementSelector, "3D"); // pass parameter group

    GridManager<GetPropType<VeinFlowTypeTag, Properties::Grid>> lowDimGridManager;
    lowDimGridManager.init("1D"); // pass parameter group
    refine1DGrid(lowDimGridManager.grid());
    auto lowDimGridData = lowDimGridManager.getGridData();

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGridManager.grid().leafGridView();
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using BulkGridGeometry = GetPropType<TissueFlowTypeTag, Properties::GridGeometry>;
    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    using LowDimGridGeometry = GetPropType<VeinFlowTypeTag, Properties::GridGeometry>;
    auto lowDimGridGeometry = std::make_shared<LowDimGridGeometry>(lowDimGridView);
    std::cout << "Bounding box of vein grid\n"
              << " -- Lower left: " << lowDimGridGeometry->bBoxMin() << "\n"
              << " -- Upper right: " << lowDimGridGeometry->bBoxMax() << std::endl;

    const auto gridSetupTime = timer.elapsed();
    std::cout << "Setting up grid geometry took " << timer.elapsed() << " seconds." << std::endl;


    // the mixed dimension type traits
    using FlowMDTraits = MultiDomainTraits<TissueFlowTypeTag, VeinFlowTypeTag>;
    constexpr auto tissueIdx = FlowMDTraits::template SubDomain<0>::Index();
    constexpr auto veinIdx = FlowMDTraits::template SubDomain<1>::Index();

    // the coupling manager
    using CouplingManager = GetPropType<TissueFlowTypeTag, Properties::CouplingManager>;
    auto couplingManager = std::make_shared<CouplingManager>(bulkGridGeometry, lowDimGridGeometry);

    // the problem (initial and boundary conditions)
    using TissueFlowProblem = GetPropType<TissueFlowTypeTag, Properties::Problem>;
    auto tissueFlowProblem = std::make_shared<TissueFlowProblem>(bulkGridGeometry, couplingManager);
    using VeinFlowProblem = GetPropType<VeinFlowTypeTag, Properties::Problem>;
    auto veinFlowProblem = std::make_shared<VeinFlowProblem>(lowDimGridGeometry, couplingManager, lowDimGridData);

    // the solution vector
    FlowMDTraits::SolutionVector sol;

    // initial solution
    tissueFlowProblem->applyInitialSolution(sol[tissueIdx]);
    veinFlowProblem->applyInitialSolution(sol[veinIdx]);

    // extracting/storing raw pointer in couplingManager to avoid shared_ptr circular references and ownership issues
    couplingManager->init(tissueFlowProblem, veinFlowProblem, sol);
    tissueFlowProblem->computePointSourceMap();
    veinFlowProblem->computePointSourceMap();

    // the grid variables
    using TissueGridVariables = GetPropType<TissueFlowTypeTag, Properties::GridVariables>;
    auto tissueGridVariables = std::make_shared<TissueGridVariables>(tissueFlowProblem, bulkGridGeometry);
    tissueGridVariables->init(sol[tissueIdx]);
    using VeinGridVariables = GetPropType<VeinFlowTypeTag, Properties::GridVariables>;
    auto veinGridVariables = std::make_shared<VeinGridVariables>(veinFlowProblem, lowDimGridGeometry);
    veinGridVariables->init(sol[veinIdx]);

    // initialize the VTK output modules for all sub-models
    const bool enableVtkOutput = getParam<bool>("Vtk.EnableOutput", true);
    using TissueSolution = std::decay_t<decltype(sol[tissueIdx])>;
    VtkOutputModule<TissueGridVariables, TissueSolution> tissueVtkWriter(*tissueGridVariables, sol[tissueIdx], tissueFlowProblem->name());
    GetPropType<TissueFlowTypeTag, Properties::IOFields>::initOutputModule(tissueVtkWriter);
    tissueVtkWriter.addField(tissueFlowProblem->spatialParams().getTissueTypes(), "tissueType");
    // if (enableVtkOutput) tissueVtkWriter.write(0.0);

    using VeinSolution = std::decay_t<decltype(sol[veinIdx])>;
    VtkOutputModule<VeinGridVariables, VeinSolution> veinVtkWriter(*veinGridVariables, sol[veinIdx], veinFlowProblem->name());
    GetPropType<VeinFlowTypeTag, Properties::IOFields>::initOutputModule(veinVtkWriter);
    veinVtkWriter.addVelocityOutput(std::make_shared<
        PorousMediumFlowVelocityOutput<VeinGridVariables, GetPropType<VeinFlowTypeTag, Properties::FluxVariables>>
    >(*veinGridVariables));
    veinVtkWriter.addField(veinFlowProblem->spatialParams().getRadii(), "radius"); // outer radius
    veinVtkWriter.addField(veinFlowProblem->spatialParams().getPermeabilities(), "K");
    // if (enableVtkOutput) veinVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<FlowMDTraits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(tissueFlowProblem, veinFlowProblem),
                                                 std::make_tuple(bulkGridGeometry, lowDimGridGeometry),
                                                 std::make_tuple(tissueGridVariables, veinGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>();

    ////////////////////////////////////////////////////////////
    // solve for the stationary velocity field
    ////////////////////////////////////////////////////////////
    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);
    
    // Bisection to find the atmospheric pressure
    // where the petiole flux becomes zero in our model
    // and in reality would become negative    
    const double fluxEpsilon = 1e-15;
    const double petioleOutsidePressure = getParam<double>("Vein.BoundaryConditions.PetiolePressure");
    double petiolePressure;
    double petioleInflux;

    const double pressureTolerance = 0.001 * 1e6; // = 0.001 MPa = 0.01 bar
    double pressureLimitLow = 1e5;
    double pressureLimitHigh = 40e5;
    double pressureApplied;
    bool converged = false;
    int iteration = -1;

    while (!converged)
    {
        // Prepare (next) iteration
        iteration++;
        pressureApplied = 0.5 * (pressureLimitLow + pressureLimitHigh);
        tissueFlowProblem->pAtmos_ = pressureApplied;
        // The initial solution guess depends on the pressure applied, if the inital guess is not good enough, the solver will not converge
        tissueFlowProblem->applyInitialSolution(sol[tissueIdx]);
        veinFlowProblem->applyInitialSolution(sol[veinIdx]);
        tissueGridVariables->update(sol[veinIdx]);
        veinGridVariables->update(sol[veinIdx]);
        couplingManager->updateSolution(sol);

        // Solve the problem with the new pressure applied
        std::cout << Fmt::format("Iteration {}: pressureLow: {:.6e}, pressureHigh: {:.6e}, pressureApplied: {:.6e}\n", iteration, pressureLimitLow, pressureLimitHigh, pressureApplied) << std::endl;
        nonLinearSolver.solve(sol);

        // Evaluate simulation: We search for a negative/zero petiole flux
        static const auto & petioleElement = veinFlowProblem->petioleElement();
        static const auto petioleIdx = veinFlowProblem->gridGeometry().elementMapper().index(petioleElement);
        petiolePressure = sol[veinIdx][petioleIdx];
        petioleInflux = petioleOutsidePressure - petiolePressure;// Flux is oriented from the outside to the petiole to the leaf
        if (petioleInflux > fluxEpsilon)
            pressureLimitLow = pressureApplied; // Increase the lower bound and therefore the middle point, because we still have a positive flux
        else
            pressureLimitHigh =  pressureApplied; // Decrease the upper bound and therefore the middle point, because the pressure was high enough to stop the flux
                                 // But we want to get closer to the point where the flux becomes zero
        std::cout << Fmt::format("Iteration {}: petioleFlux: {:.6e}\n", iteration, petioleInflux) << std::endl;

        // Check for convergence
        converged = std::abs(pressureLimitHigh - pressureLimitLow) < pressureTolerance;
        if (converged) std::cout << Fmt::format("Converged after {} iterations to the critical pressure: {:.6e}\n", iteration, pressureApplied) << std::endl;
    }

    // Results
    double leaf_area = 0.0;
    for (const auto& element : elements(bulkGridView))
    {
        const auto volume = element.geometry().volume();
        leaf_area += volume / tissueFlowProblem->height_;
    }

    // Calculate the vein density m/m^2
    double vein_length = 0.0;
    for (const auto& element : elements(lowDimGridView))
    {
        vein_length += element.geometry().volume();
    }
    const double vein_density = vein_length / leaf_area;

    auto& diffevap = tissueFlowProblem->diffusiveEvaporationRate_;
    auto& advevap = tissueFlowProblem->advectiveEvaporationRate_;
    const double total_evaporation = std::reduce(diffevap.begin(), diffevap.end()) + std::reduce(advevap.begin(), advevap.end()); //kg/s

    // sites of evaporation
    const double reference_evaporation = 0.005 * leaf_area * CouplingManager::H2O::molarMass();//source data mol/m^2/s to kg/s
    const double reference_evaporation_2 = 0.0000569*1e-3; // source data 0.0000569 g/s to kg/s

    std::cout<<std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Results" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Sum of evaporation rate vector: [kg/s] " << total_evaporation << std::endl;
    std::cout << "Diffusive evaporation rate: [kg/s] " << std::reduce(diffevap.begin(), diffevap.end()) << std::endl;
    std::cout << "Advective evaporation rate: [kg/s] " << std::reduce(advevap.begin(), advevap.end()) << std::endl;
    std::cout << "Reference evaporation rate: [kg/s] " << reference_evaporation << std::endl;
    std::cout << "Reference evaporation rate 2: [kg/s] " << reference_evaporation_2 << std::endl;
    std::cout << std::endl;
    std::cout << "Leaf area: [m^2] " << leaf_area << std::endl;
    std::cout << std::endl;
    std::cout << "AtmosMassFraction: " << tissueFlowProblem->h2oMassFractionAtmos_ << std::endl;
    std::cout << "pAtmos: " << tissueFlowProblem->pAtmos_ << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Vein density: [m/m^2] " << vein_density << std::endl;
    std::cout << "Vein density: [mm/mm^2] " << vein_density / 1e3 << std::endl;
    std::cout << std::endl;

    // write vtk output
    if (enableVtkOutput)
    {
        tissueVtkWriter.write(1.0);
        veinVtkWriter.write(1.0);
    }

    std::cout << "Flow computation took " << timer.elapsed()-gridSetupTime << " seconds." << std::endl;

    // print used/unused paramters at the end (good for debugging)
    // Parameters::print();

    return 0;
}
