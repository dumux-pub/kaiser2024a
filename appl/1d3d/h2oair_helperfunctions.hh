#ifndef DUMUX_H2OAIR_HELPER_HH
#define DUMUX_H2OAIR_HELPER_HH

#include <cmath>
#include <dumux/material/constants.hh>

namespace Dumux {

template <class H2OAir>
class H2OAirHelperFunctions
{

public:
    using Scalar = typename H2OAir::Scalar;
    using H2O = typename H2OAir::H2O;
    using Air = typename H2OAir::Air;

    static constexpr Scalar referencePressure = 1e5;
    static constexpr Scalar referenceTemperature = 293.15;

    // TODO: not tested
    template <typename FluidState, std::enable_if_t<FluidState::numPhases == 2, int> = 0>
    static Scalar waterPotential(const FluidState& fs, int phaseIdx)
    {
        if (phaseIdx == H2OAir::liquidPhaseIdx)
            return liquidWaterPotential(fs.pressure(H2OAir::liquidPhaseIdx));
        else if (phaseIdx == H2OAir::gasPhaseIdx)
            return gasWaterPotential(fs);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index");
    }

    // TODO: not tested
    static Scalar liquidWaterPotential(Scalar liquidPressure)
    {
        return liquidPressure - referencePressure;
    }
    
    // TODO: not tested
    template<typename FluidState, std::enable_if_t<FluidState::numPhases == 2, int> = 0>
    static Scalar gasWaterPotential(const FluidState& fs)
    {
        const Scalar a = fs.partialPressure(H2OAir::gasPhaseIdx, H2OAir::H2OIdx) / H2O::vaporPressure(fs.temperature(H2OAir::gasPhaseIdx));
        const Scalar T = fs.temperature(H2OAir::gasPhaseIdx);
        return kelvinPressure(T, a);
    }

    /**
     * @brief Calculate the capillary pressure for a given pore radius according to the Young-Laplace equation
     * 
     * @param r The pore radius
     * @return Scalar The capillary pressure, greater than zero
     */
    static Scalar capillaryPressure(Scalar r, Scalar theta = 25.0 * M_PI / 180.0, Scalar sigma = 0.0728)
    {
        return 2.*sigma*std::cos(theta)/r;
    }

    /**
     * @brief Calculate the kelvin pressure / potential for a gas
     * 
     * @param T The temperature of the gas
     * @param a The activity / relative humidity of the gas
     * @return Scalar The kelvin pressure / potential
     */
    static Scalar kelvinPressure(Scalar T, Scalar a)
    {
        static const Scalar Vw = 1./H2O::liquidMolarDensity(referenceTemperature, referencePressure);
        if (a < 0.0 || a > 1.0) std::cerr << "Warning: Invalid activity value in kelvinPressure: " << a << std::endl;
        a = std::clamp(a, 0.0, 1.0);
        return Constants<Scalar>::R * T / Vw * std::log(a);
    }

    /**
     * @brief Calculate the mass fraction of water in the air based relative humidity, the air pressure and the temperature
     * 
     * @param Hrel The relative humidity
     * @param p The air pressure
     * @param T The temperature
     * @return Scalar The mass fraction of water in the air
     */
    static Scalar massFraction(Scalar Hrel, Scalar p, Scalar T)
    {
        Scalar x_h2o_tissue = Hrel * H2O::vaporPressure(T)/p;
        Scalar averageMolarMass = H2O::molarMass() * x_h2o_tissue + Air::molarMass() * (1.0 - x_h2o_tissue);
        Scalar X_h2o_tissue = x_h2o_tissue * H2O::molarMass() / averageMolarMass;
        return X_h2o_tissue;
    }

    /**
     * @brief Calculate the mole fraction of water in the air based relative humidity, the air pressure and the temperature
     * 
     * @param Hrel The relative humidity
     * @param p The air pressure
     * @param T The temperature
     * @return Scalar The mole fraction of water in the air
     */
    static Scalar moleFraction(Scalar Hrel, Scalar p, Scalar T)
    {
        return Hrel * H2O::vaporPressure(T)/p;
    }

    /**
     * @brief Calculate the saturation mole fraction of a fluid in a pore
     * 
     * @param fs The fluid state
     * @return Scalar The saturation mole fraction
     */
    template<typename FluidState>
    static Scalar saturationMoleFraction(FluidState const & fs)
    {
        const Scalar pv = H2OAir::vaporPressure(fs, H2OAir::H2OIdx);
        const Scalar p = fs.pressure(H2OAir::gasPhaseIdx);
        return pv/p;
    }

};

} // namespace Dumux

#endif // DUMUX_H2OAIR_HELPER_HH