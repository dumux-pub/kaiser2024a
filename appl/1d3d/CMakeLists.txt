set(CMAKE_BUILD_TYPE Release)
# set(CMAKE_BUILD_TYPE Debug)
dune_symlink_to_source_files(FILES "grids" "inputs")
dumux_add_test(NAME leaf_evaporation
    SOURCES main.cc
)

dumux_add_test(NAME pressure_bomb
    SOURCES pressure_bomb.cc
)

dumux_add_test(NAME test_leaf_evaporation
    SOURCES test_leaf_evaporation.cc
)
