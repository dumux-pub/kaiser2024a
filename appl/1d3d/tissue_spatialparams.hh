// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TISSUE_SPATIAL_PARAMS_HH
#define DUMUX_TISSUE_SPATIAL_PARAMS_HH

#include <dumux/common/parameters.hh>
#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

template<class FVGridGeometry, class Scalar>
class TissueSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP<FVGridGeometry, Scalar, TissueSpatialParams<FVGridGeometry, Scalar>>
{
    using ThisType = TissueSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<FVGridGeometry, Scalar, ThisType>;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    TissueSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        permeability_ = getParam<Scalar>("Tissue.SpatialParams.Permeability");
        porosity_ = getParam<Scalar>("Tissue.SpatialParams.Porosity");
        temperature_ = getParam<Scalar>("Tissue.SpatialParams.Temperature");

        if ("SapFluid" != getParam<std::string>("1.Component.Name"))
            DUNE_THROW(ParameterException, "Assuming 1st component is sap fluid!");

        calculateTissueTypes();
    }

    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return temperature_;
    }

    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        return permeability_;
    }

    enum TissueType {
        Palisaide = 0,
        Spongy = 1,
        AirSpace = 2
    };

    TissueType tissueTypeAtPos(const GlobalPosition& globalPos) const
    {
        // We divide the leaf tissue into three vertically aranged layers, each with different porosity
        // top, pallisade, occupy 35% of the leaf thickness: porosity = 0.4
        // middle, spongy, occupy 55% of the leaf thickness: porosity = 0.45
        // bottom, air space over the stomata, occupy 10% of the leaf thickness: porosity = 0.9
        static const Scalar top = this->gridGeometry().bBoxMax()[2];
        static const Scalar bottom = this->gridGeometry().bBoxMin()[2];
        static const Scalar thickness = top - bottom;
        const Scalar zRel = (globalPos[2] - bottom) / thickness;
        if(zRel < 0.1)
            return TissueType::AirSpace;
        else if(zRel < 0.65)
            return TissueType::Spongy;
        else
            return TissueType::Palisaide;

    }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        switch (tissueTypeAtPos(globalPos))
        {
            case Palisaide:
                return 0.4;
            case Spongy:
                return 0.45;
            case AirSpace:
                return 0.9;
        }
        return 0;
    }

    void calculateTissueTypes()
    {
        const auto& gg = this->gridGeometry();
        auto numElements = gg.gridView().size(0);
        tissueType_.resize(numElements);

        for (const auto& element : elements(gg.gridView()))
        {
            auto eIdx = gg.elementMapper().index(element);
            auto center = element.geometry().center();
            auto tT = static_cast<int>(tissueTypeAtPos(center));
            tissueType_[eIdx] = tT;
        }
    }

    const std::vector<int>& getTissueTypes() const
    { return tissueType_; }

private:
    Scalar permeability_;
    Scalar porosity_;
    Scalar temperature_;
    std::vector<int> tissueType_;
};

} // end namespace Dumux

#endif
