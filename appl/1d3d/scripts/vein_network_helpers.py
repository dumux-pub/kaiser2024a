from collections import Counter
import numpy as np

# Removes all veins with a radius lesser than the given threshold from a dgf file
def remove_smaller_veins(grid_file: str, threshold: float, output_file: str):
    with open(grid_file, "r") as file:
        with open(output_file, "w") as output:
            if file.readline().strip() != "DGF":
                raise ValueError("Not a DGF file")
            output.write("DGF\n")
            if file.readline().strip() != "Vertex":
                raise ValueError("Expected Vertex")
            output.write("Vertex\n")
            # copy the vertices
            line = file.readline().strip()
            while line != "#":
                output.write(line + "\n")
                line = file.readline().strip()
            output.write("#\n")
            # now the vertices indices together with the radius are read
            if file.readline().strip() != "SIMPLEX":
                raise ValueError("Expected SIMPLEX")
            output.write("SIMPLEX\n")
            if file.readline().strip() != "parameters 1":
                raise ValueError("Expected parameters 1")
            output.write("parameters 1\n")
            # calculate mean_radius, element count and the radii distribution on the fly
            r_counter = Counter()
            while True:
                line = file.readline().strip()
                if not line or line == "#":
                    break
                # we get two ints and a float
                id1, id2, radius = line.split()
                id1, id2, radius = int(id1), int(id2), float(radius)
                if radius >= threshold:
                    output.write(f"{id1} {id2} {radius}\n")
                    r_counter[radius] += 1
            output.write("#\n")
            n_elements = sum(r_counter.values())
            mean_radius = sum([r * c for r, c in r_counter.items()]) / n_elements
            unique_radii = set(r_counter.keys())
            print("Mean radius of the small_leaf network: ", mean_radius)
            print("Number of elements: ", n_elements)
            print("Unique radii: ", unique_radii)
            print("Number of unique radii: ", len(unique_radii))
            return r_counter


# read first line, should be "DGF"
def mean_diameter(grid_file: str):
    vertices = []
    elements = []
    with open(grid_file, "r") as file:
        if file.readline().strip() != "DGF":
            raise ValueError("Not a DGF file")
        # next line should be "Vertex"
        if file.readline().strip() != "Vertex":
            raise ValueError("Expected Vertex")
        line = file.readline().strip()
        while line != "#":
            # add the 3d coordinates to the list
            vertices.append(np.array(list(map(float, line.split()))))
            line = file.readline().strip()
        # now the vertices indices together with the radius are read
        if file.readline().strip() != "SIMPLEX":
            raise ValueError("Expected SIMPLEX")
        if file.readline().strip() != "parameters 1":
            raise ValueError("Expected parameters 1")
        # calculate the mean diameter on the fly and count the occurences of unique radii
        n_elements = 0
        mean_radius = 0
        unique_radii = set()
        while True:
            line = file.readline().strip()
            if not line or line == "#":
                break
            n_elements += 1
            id1, id2, radius = map(float, line.split())
            elements.append(np.array([id1, id2, radius]))
            unique_radii.add(radius)
            mean_radius += radius
        mean_radius /= n_elements
        return mean_radius, unique_radii