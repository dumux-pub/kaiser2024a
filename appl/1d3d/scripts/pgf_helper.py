import matplotlib

def usePGF():
    # matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        # 'pgf.rcfonts': False,
        'font.family': 'serif',
        # use latex font computer modern
        'text.usetex': True,
        # fontsize
        'font.size': 18, # because i put them in subplots, they get small in the tex document
        # "figure.figsize": figsize(0.9),     # default fig size of 0.9 textwidth
        "pgf.preamble": "\n".join([
            r"\usepackage[utf8x]{inputenc}",
            r"\usepackage[T1]{fontenc}",
            r"\usepackage{amsmath}",
            r"\usepackage{amssymb}",
            r"\usepackage{siunitx}",
        ])
    })
def useDefault():
    matplotlib.rcParams.update(matplotlib.rcParamsDefault)