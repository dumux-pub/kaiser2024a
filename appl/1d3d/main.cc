// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Multi-domain vein-leaf model
 */
#include <config.h>

#include <dune/common/timer.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/geometry/distance.hh>
#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/container.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/grid/gridmanager_foam.hh>

#include <dumux/adaptive/markelements.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include "properties.hh"
#include "../veins/leafshape.hh"

#include "h2oair_helperfunctions.hh"

namespace Dumux {

// helper function to make sure both grid have roughly the same element sizes (h_max)
template<class Grid1D>
void refine1DGrid(Grid1D& grid1D)
{
    const auto& gridView = grid1D.leafGridView();
    const auto lowerLeft = getParam<Dune::FieldVector<double, 3>>("3D.Grid.LowerLeft");
    const auto upperRight = getParam<Dune::FieldVector<double, 3>>("3D.Grid.UpperRight");
    const auto length = upperRight - lowerLeft;
    const auto cells = getParam<Dune::FieldVector<double, 3>>("3D.Grid.Cells");
    const auto factor = getParam<double>("1D.Grid.MaxElementLengthFactor", 1.0);
    const auto maxElementLength = std::max({length[0]/cells[0], length[1]/cells[1], length[2]/cells[2]})*factor;
    std::cout << Fmt::format(
        "Refining grid with {} elements until max length: {:.2e}\n",
        gridView.size(0), maxElementLength
    );

    bool markedElements = true;
    while (markedElements)
    {
        markedElements = markElements(
            grid1D,
            [&](const auto& element){
                return element.geometry().volume() > maxElementLength ? 1 : 0;
            },
            false
        );

        if (markedElements)
        {
            grid1D.preAdapt();
            grid1D.adapt();
            grid1D.postAdapt();
        }
    }

    auto maxLength = 0.0;
    auto minLength = 1.0;
    for (const auto& element : elements(gridView))
    {
        const auto length = element.geometry().volume();
        maxLength = std::max(maxLength, length);
        minLength = std::min(minLength, length);
    }

    std::cout << Fmt::format("After refinement: {} elements\n", gridView.size(0))
              << Fmt::format("  -- max length: {:.2e}\n", maxLength)
              << Fmt::format("  -- min length: {:.2e}\n", minLength)
              << Fmt::format("  -- max level: {}\n", grid1D.maxLevel());
}

} // end namespace Dumux

int main(int argc, char** argv)
{
    using namespace Dumux;

    Dumux::initialize(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

     // Define the sub problem type tags
    using TissueFlowTypeTag = Properties::TTag::Tissue;
    using VeinFlowTypeTag = Properties::TTag::VeinFlow;

    // start the timer
    Dune::Timer timer;

    // leaf shape for subgrid
    const double finalLeafHeight = getParam<double>("LeafShape.ApexHeight");
    const double initialLeafScale = getParam<double>("LeafGrowth.InitialLeafHeight")/finalLeafHeight;
    LeafVeinGenerator::LeafShape<3> leafShape;
    leafShape.setScale(initialLeafScale);

    // cut out elements within the stair-case region
    auto elementSelector = [&](const auto& element)
    {
        // const auto globalPos = element.geometry().center();
        // const auto x = globalPos[0];
        // const auto y = globalPos[1];

        const auto apexHeight = 1e-3*leafShape.bBoxMax()[1];
        const auto petioleHeight = 1e-3*leafShape.bBoxMin()[1];
        // const auto centerWidth = 1e-3*leafShape.width(y*1e3);
        // if (y < apexHeight && y > petioleHeight && x < centerWidth && x > -centerWidth)
        //     return true;

        // improved version: check if any of the four corners of the element is inside the leaf
        const int corners = element.geometry().corners();
        for (int i = 0; i < corners; ++i)
        {
            const auto localPos = element.geometry().corner(i);
            const auto x = localPos[0];
            const auto y = localPos[1];
            const auto width = 1e-3*leafShape.width(y*1e3);
            if (y < apexHeight && y > petioleHeight && x < width && x > -width)
                return true;
        }
        return false;
    };

    auto allElementSelector = [&](const auto& element)
    {
        return true;
    };

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    GridManager<GetPropType<TissueFlowTypeTag, Properties::Grid>> bulkGridManager;

    static const bool leafShapeEnabled = getParam<bool>("LeafShape.LeafShapeEnabled", true);
    if (leafShapeEnabled)
    {
        bulkGridManager.init(elementSelector, "3D"); // pass parameter group
    }
    else
    {
        bulkGridManager.init(allElementSelector, "3D"); // pass parameter group
    }

    GridManager<GetPropType<VeinFlowTypeTag, Properties::Grid>> lowDimGridManager;
    lowDimGridManager.init("1D"); // pass parameter group
    refine1DGrid(lowDimGridManager.grid());
    auto lowDimGridData = lowDimGridManager.getGridData();

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGridManager.grid().leafGridView();
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using BulkGridGeometry = GetPropType<TissueFlowTypeTag, Properties::GridGeometry>;
    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    using LowDimGridGeometry = GetPropType<VeinFlowTypeTag, Properties::GridGeometry>;
    auto lowDimGridGeometry = std::make_shared<LowDimGridGeometry>(lowDimGridView);
    std::cout << "Bounding box of vein grid\n"
              << " -- Lower left: " << lowDimGridGeometry->bBoxMin() << "\n"
              << " -- Upper right: " << lowDimGridGeometry->bBoxMax() << std::endl;

    const auto gridSetupTime = timer.elapsed();
    std::cout << "Setting up grid geometry took " << timer.elapsed() << " seconds." << std::endl;


    // the mixed dimension type traits
    using FlowMDTraits = MultiDomainTraits<TissueFlowTypeTag, VeinFlowTypeTag>;
    constexpr auto tissueIdx = FlowMDTraits::template SubDomain<0>::Index();
    constexpr auto veinIdx = FlowMDTraits::template SubDomain<1>::Index();

    // the coupling manager
    using CouplingManager = GetPropType<TissueFlowTypeTag, Properties::CouplingManager>;
    auto couplingManager = std::make_shared<CouplingManager>(bulkGridGeometry, lowDimGridGeometry);

    // the problem (initial and boundary conditions)
    using TissueFlowProblem = GetPropType<TissueFlowTypeTag, Properties::Problem>;
    auto tissueFlowProblem = std::make_shared<TissueFlowProblem>(bulkGridGeometry, couplingManager);
    using VeinFlowProblem = GetPropType<VeinFlowTypeTag, Properties::Problem>;
    auto veinFlowProblem = std::make_shared<VeinFlowProblem>(lowDimGridGeometry, couplingManager, lowDimGridData);

    // the solution vector
    FlowMDTraits::SolutionVector sol;

    // initial solution
    tissueFlowProblem->applyInitialSolution(sol[tissueIdx]);
    veinFlowProblem->applyInitialSolution(sol[veinIdx]);

    // extracting/storing raw pointer in couplingManager to avoid shared_ptr circular references and ownership issues
    couplingManager->init(tissueFlowProblem, veinFlowProblem, sol);
    tissueFlowProblem->computePointSourceMap();
    veinFlowProblem->computePointSourceMap();

    // the grid variables
    using TissueGridVariables = GetPropType<TissueFlowTypeTag, Properties::GridVariables>;
    auto tissueGridVariables = std::make_shared<TissueGridVariables>(tissueFlowProblem, bulkGridGeometry);
    tissueGridVariables->init(sol[tissueIdx]);
    using VeinGridVariables = GetPropType<VeinFlowTypeTag, Properties::GridVariables>;
    auto veinGridVariables = std::make_shared<VeinGridVariables>(veinFlowProblem, lowDimGridGeometry);
    veinGridVariables->init(sol[veinIdx]);

    // initialize the VTK output modules for all sub-models
    const bool enableVtkOutput = getParam<bool>("Vtk.EnableOutput", true);
    using TissueSolution = std::decay_t<decltype(sol[tissueIdx])>;
    VtkOutputModule<TissueGridVariables, TissueSolution> tissueVtkWriter(*tissueGridVariables, sol[tissueIdx], tissueFlowProblem->name());
    GetPropType<TissueFlowTypeTag, Properties::IOFields>::initOutputModule(tissueVtkWriter);
    tissueVtkWriter.addField(tissueFlowProblem->spatialParams().getTissueTypes(), "tissueType");
    // if (enableVtkOutput) tissueVtkWriter.write(0.0);

    using VeinSolution = std::decay_t<decltype(sol[veinIdx])>;
    VtkOutputModule<VeinGridVariables, VeinSolution> veinVtkWriter(*veinGridVariables, sol[veinIdx], veinFlowProblem->name());
    GetPropType<VeinFlowTypeTag, Properties::IOFields>::initOutputModule(veinVtkWriter);
    veinVtkWriter.addVelocityOutput(std::make_shared<
        PorousMediumFlowVelocityOutput<VeinGridVariables, GetPropType<VeinFlowTypeTag, Properties::FluxVariables>>
    >(*veinGridVariables));
    veinVtkWriter.addField(veinFlowProblem->spatialParams().getRadii(), "radius"); // outer radius
    veinVtkWriter.addField(veinFlowProblem->spatialParams().getPermeabilities(), "K");

    // Add water potential to the outputs
    using OnePAdapter = GetPropType<TissueFlowTypeTag, Properties::FluidSystem>;
    using H2OAir = typename OnePAdapter::MultiPhaseFluidSystem;
    using H2OAirHelper = H2OAirHelperFunctions<H2OAir>;
    veinVtkWriter.addVolumeVariable([](const auto& v){ return H2OAirHelper::liquidWaterPotential(v.pressure()); }, "waterPotential");
    auto H2OIdx = OnePAdapter::compIdx(H2OAir::H2OIdx);
    auto gasPhaseIdx = OnePAdapter::phase0Idx;
    tissueVtkWriter.addVolumeVariable([H2OIdx, gasPhaseIdx](const auto& v){
        const auto fs = v.fluidState();
        const OnePAdapter::Scalar a = fs.partialPressure(gasPhaseIdx, H2OIdx) / H2OAir::H2O::vaporPressure(fs.temperature(gasPhaseIdx));
        const OnePAdapter::Scalar T = fs.temperature(gasPhaseIdx);
        return H2OAirHelper::kelvinPressure(T, a);
    }, "waterPotential");

    tissueVtkWriter.addVolumeVariable([H2OIdx, gasPhaseIdx](const auto& v){
        const auto fs = v.fluidState();
        return fs.partialPressure(gasPhaseIdx, H2OIdx) / H2OAir::H2O::vaporPressure(fs.temperature(gasPhaseIdx));
    }, "relativeHumidity");

    // if (enableVtkOutput) veinVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<FlowMDTraits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(tissueFlowProblem, veinFlowProblem),
                                                 std::make_tuple(bulkGridGeometry, lowDimGridGeometry),
                                                 std::make_tuple(tissueGridVariables, veinGridVariables),
                                                 couplingManager);

    // the linear solver
    // using LinearSolver = UMFPackIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    using LinearSolver =  BlockDiagAMGBiCGSTABSolver;
    auto linearSolver = std::make_shared<LinearSolver>();

    ////////////////////////////////////////////////////////////
    // solve for the stationary velocity field
    ////////////////////////////////////////////////////////////
    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);
    nonLinearSolver.solve(sol);

    // Results
    const auto & radii = veinFlowProblem->spatialParams().getRadii();
    double max_radius = *std::max_element(radii.begin(), radii.end());
    double min_radius = *std::min_element(radii.begin(), radii.end());

    double leaf_area = tissueFlowProblem->leafArea();
    double leaf_volume = leaf_area * tissueFlowProblem->height();

    // Calculate the vein density m/m^2
    double vein_length = veinFlowProblem->veinLength();
    const double vein_density = vein_length / leaf_area;

    // Calculate the vein volume density m^3/m^3
    double vein_volume = veinFlowProblem->veinVolume();
    const double vein_volume_density = vein_volume / leaf_volume;

    auto& diffevap = tissueFlowProblem->diffusiveEvaporationRate_;
    auto& advevap = tissueFlowProblem->advectiveEvaporationRate_;
    const double total_evaporation = std::reduce(diffevap.begin(), diffevap.end()) + std::reduce(advevap.begin(), advevap.end()); //kg/s

    // sites of evaporation
    const double reference_evaporation = 0.005 * leaf_area * CouplingManager::H2O::molarMass();//source data mol/m^2/s to kg/s
    const double reference_evaporation_2 = 0.0000569*1e-3; // source data 0.0000569 g/s to kg/s

    // Petiole pressure
    auto & petioleElement = veinFlowProblem->petioleElement();
    auto petioleIdx = veinFlowProblem->gridGeometry().elementMapper().index(petioleElement);

    std::cout<<std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Results" << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << std::fixed << std::setprecision(10) << std::endl; // Increase output precision for the following output
    std::cout << "petioleIdx: " << petioleIdx << std::endl;
    std::cout << "Pressure at petiole outlet: [Pa] " << veinFlowProblem->petiolePressure() << std::endl;
    std::cout << "Petiole pressure at first element:  [Pa] " << sol[veinIdx][petioleIdx][0] << std::endl;
    std::cout << "Petiole pressure at second element: [Pa] " << sol[veinIdx][petioleIdx+1][0] << std::endl;
    std::cout << std::defaultfloat << std::setprecision(6) << std::endl; // Reset output precision
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Sum of evaporation rate vector: [kg/s] " << total_evaporation << std::endl;
    std::cout << "Diffusive evaporation rate: [kg/s] " << std::reduce(diffevap.begin(), diffevap.end()) << std::endl;
    std::cout << "Advective evaporation rate: [kg/s] " << std::reduce(advevap.begin(), advevap.end()) << std::endl;
    std::cout << "Reference evaporation rate: [kg/s] " << reference_evaporation << std::endl;
    std::cout << "Reference evaporation rate 2: [kg/s] " << reference_evaporation_2 << std::endl;
    std::cout << std::endl;
    static const double bundleSheathPoreRadius = getParam<double>("Interfaces.BundleSheathPoreRadius");
    std::cout << "Maximal capillary pressure in cell wall: [Pa] " << H2OAirHelper::capillaryPressure(bundleSheathPoreRadius) << std::endl;
    std::cout << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "TAtmos: " << tissueFlowProblem->TAtmos_ << std::endl;
    std::cout << "pAtmos: " << tissueFlowProblem->pAtmos_ << std::endl;
    std::cout << "Atmos Hrel: " << tissueFlowProblem->Hrel_ << std::endl;
    std::cout << "AtmosMassFraction: " << tissueFlowProblem->h2oMassFractionAtmos_ << std::endl;
    std::cout << "AtmosphereWaterPotential: " << Constants<double>::R * H2OAir::H2O::liquidMolarDensity(298.15, 1e5) * tissueFlowProblem->TAtmos_ * std::log(tissueFlowProblem->Hrel_) << std::endl;
    std::cout << "----------------------------------------" << std::endl;
    std::cout << "Vein Length: [m] " << vein_length <<  " | " << vein_length*1e3 << " mm" << std::endl;
    std::cout << "Vein Surface: [m^2] " << veinFlowProblem->veinSurface() << " | " << veinFlowProblem->veinSurface()*1e6 << " mm^2" << std::endl;
    std::cout << "Vein Volume: [m^3] " << vein_volume << " | " << vein_volume*1e9 << " mm^3" << std::endl;
    std::cout << "Minimal vein radius: [m] " << min_radius << " | " << min_radius*1e3 << " mm" << " | " << min_radius*1e6 << " µm" << std::endl;
    std::cout << "Maximal vein radius: [m] " << max_radius << " | " << max_radius*1e3 << " mm" << " | " << max_radius*1e6 << " µm" << std::endl;
    std::cout << std::endl;
    std::cout << "Leaf height: [m] " << tissueFlowProblem->height() << " | " << tissueFlowProblem->height()*1e3 << " mm" <<  " | " << tissueFlowProblem->height()*1e6 << " µm" << std::endl; // "Estimated: 193e-6 m"
    std::cout << "Leaf area: [m^2] " << leaf_area << " | " << leaf_area*1e6 << " mm^2" << std::endl; //Measurements: 0.002175148 m^2
    std::cout << "Leaf volume: [m^3] " << leaf_volume << " | " << leaf_volume*1e9 << " mm^3" << std::endl;
    std::cout << std::endl;
    std::cout << "Vein density: [m/m^2] " << vein_density << std::endl;
    std::cout << "Vein density: [mm/mm^2] " << vein_density / 1e3 << std::endl;
    std::cout << "Vein volume density: [m^3/m^3] " << vein_volume_density << std::endl;
    std::cout << std::endl;

    // write vtk output
    if (enableVtkOutput)
    {
        tissueVtkWriter.write(1.0);
        veinVtkWriter.write(1.0);
    }

    std::cout << "Flow computation took " << timer.elapsed()-gridSetupTime << " seconds." << std::endl;

    // print used/unused paramters at the end (good for debugging)
    // Parameters::print();

    return 0;
}
