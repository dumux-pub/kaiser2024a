// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_VEIN_FLOW_SPATIALPARAMS_HH
#define DUMUX_VEIN_FLOW_SPATIALPARAMS_HH

#include <dumux/common/parameters.hh>
#include <dumux/porousmediumflow/fvspatialparams1p.hh>

namespace Dumux {

/*!
 * \ingroup OneTests
 * \brief Definition of the spatial parameters for the blood flow problem
 */
template<class GridGeometry, class Scalar>
class VeinFlowSpatialParams
: public FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, VeinFlowSpatialParams<GridGeometry, Scalar>>
{
    using ThisType = VeinFlowSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsOneP<GridGeometry, Scalar, ThisType>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    VeinFlowSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        const auto permeability = getParam<Scalar>("Vein.SpatialParams.Permeability");
        permeability_.assign(gridGeometry->gridView().size(0), permeability);
        porosity_ = getParam<Scalar>("Vein.SpatialParams.Porosity");
        temperature_ = getParam<Scalar>("Vein.SpatialParams.Temperature");

        if ("SapFluid" != getParam<std::string>("1.Component.Name"))
            DUNE_THROW(ParameterException, "Assuming 1st component is sap fluid!");
    }

    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        const auto radius = this->circleEquivalentRadius(eIdx);
        return M_PI*radius*radius;
    }

    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        return permeability_[scv.elementIndex()];
    }

    // Return the "circle-equivalent" radius
    Scalar circleEquivalentRadius(std::size_t eIdx) const
    { return veinRadius_[eIdx]; }

    // Return the cross-sectional outer perimeter of the PVS annulus
    Scalar perimeter(std::size_t eIdx) const
    { return 2*M_PI*veinRadius_[eIdx]; }

    // Return the outer radius
    Scalar radius(std::size_t eIdx) const
    { return veinRadius_[eIdx]; }

    //! Porosity of the veins
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    //! Temperature of the veins
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    { return temperature_; }

    //! Get the radii for e.g. output
    const std::vector<Scalar>& getRadii() const
    { return veinRadius_; }

    //! Get the permeabilities for output
    const std::vector<Scalar>& getPermeabilities() const
    { return permeability_; }

    //! Read params from dgf
    template<class GridData>
    void readGridParams(const GridData& gridData)
    {
        const auto& gg = this->gridGeometry();
        auto numElements = gg.gridView().size(0);
        veinRadius_.resize(numElements);

        // gridview is a leafGridView. Parameters are only set on level 0.
        // elements have to inherit spatial parameters from their father.
        for (const auto& element : elements(gg.gridView()))
        {
            auto level0element = element;
            while(level0element.hasFather())
                level0element = level0element.father();

            auto eIdx = gg.elementMapper().index(element);
            veinRadius_[eIdx] = gridData.parameters(level0element)[0];
        }
    }

private:
    std::vector<Scalar> veinRadius_;
    std::vector<Scalar> permeability_;
    Scalar porosity_;
    Scalar temperature_;
};

} // end namespace Dumux

#endif
